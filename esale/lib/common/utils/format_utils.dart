import 'package:esale/common/utils/log_util.dart';
import 'package:esale/common/extentions/string_extensions.dart';
import 'package:intl/intl.dart';

class FormatUtils {
  static String formatCurrencyDoubleToString(double currency,
      {bool haveUnit = true}) {
    try {
      if (currency == null || currency == 0) return '';
      final output = NumberFormat.simpleCurrency(locale: 'vi').format(currency);
      return haveUnit ? output : output.trim().replaceAll('₫', '');
    } catch (e) {
      LOG.e('Exception: formatCurrencyDoubleToString: ${e.toString()}');
      return '$currency';
    }
  }

  static String formatCurrencyForDisplay(String input) {
    double format = formatMoneyFromStringTextField(input);
    String result = NumberFormat.simpleCurrency(locale: 'vi').format(format);
    return result.substring(0, result.length - 2);
  }

  static double formatMoneyFromStringTextField(String input) {
    try {
      if (input.isEmptyOrNull) return 0.0;
      return double.tryParse(input.replaceAll('.', ''));
    } catch (e) {
      return 0.0;
    }
  }

  static String formatSpaceToDisplay(double input) {
    if (input == null) return '';
    if (input - input.toInt() == 0) {
      return '$input';
    }
    if (input > 10) {
      return '${input.round()}';
    }
    return '${input}0000'.substring(0, 3);
  }

  static String formatAvageStar(double input) {
    if (input == null) return '';
    if (input - input.toInt() == 0) {
      return '$input.0';
    }
    return '${input}0000'.substring(0, 3);
  }

  static String convertSecondToTextHourMinute(int seconds) {
    if (seconds == null) {
      return null;
    }

    if (seconds < 60) {
      return '$seconds giây';
    }
    int hour = (seconds / 3600).round();
    int minute = ((seconds - hour * 3600) / 60).round();
    String textHour = '${hour != 0 ? '${hour}giờ' : ''}';
    String minuteText = '${minute != 0 ? '${minute}phút' : ''}';
    return '$textHour$minuteText';
  }

  static int formatTextPriceToDouble(String input) {
    final format = input.replaceAll('đ', '').replaceAll('.', '').trim();
    return int.parse(format);
  }

  static String formatDateTimeString(String input) {
    try {
      DateFormat dateFormat = DateFormat("yyyy-MM-ddTHH:mm:ss");
      DateFormat dateFormatLast = DateFormat("HH:mm - dd/MM/yyyy");
      DateTime datetime = dateFormat.parse(input);
      return dateFormatLast.format(datetime);
    } catch (e) {
      return input;
    }
  }

  static String formatDateTimeBySource(DateTime input, {String source}) {
    try {
      DateFormat dateFormatLast = DateFormat(source ?? "HH:mm - dd/MM/yyyy");
      return dateFormatLast.format(input);
    } catch (e) {
      return '$input';
    }
  }

  static DateTime convertServerDateTime(double miliseconds) {
    DateTime dateDefault = DateTime(2019, 1, 1);
    return DateTime.fromMillisecondsSinceEpoch(
        dateDefault.millisecondsSinceEpoch + miliseconds.toInt() * 1000);
  }

  static String formatTimeDisplay(int time) {
    int minute = 0;
    String minuteText = "";
    int second = 0;
    String secondText = "";

    minute = time ~/ 60;
    second = time % 60;
    if (minute < 10) {
      minuteText = "0$minute";
    } else {
      minuteText = "$minute";
    }

    if (second < 10) {
      secondText = "0$second";
    } else {
      secondText = "$second";
    }
    return minuteText + ":" + secondText;
  }
}
