import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/enum.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/alert_dialog_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;

import 'package:tiengviet/tiengviet.dart';

const defaultMarginTopOfBottomSheet = 44.0;

class CommonUtil {
  static void showCustomBottomSheet({
    @required BuildContext context,
    @required Widget child,
    double height,
    Function onClosed,
    EdgeInsets margin,
    Color backgroundColor,
  }) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (BuildContext builder) {
        double _maxHeight = ScreenUtil.screenHeightDp -
            defaultMarginTopOfBottomSheet -
            ScreenUtil.statusBarHeight;
        return Container(
          height: height != null ? min(height, _maxHeight) : _maxHeight,
          margin: margin ?? EdgeInsets.all(0),
          decoration: BoxDecoration(
            color: backgroundColor ?? Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12), topRight: Radius.circular(12)),
          ),
          child: child,
        );
      },
    ).whenComplete(() {
      if (onClosed != null) {
        onClosed();
      }
    });
  }

  static dynamic getObjectInList(List<dynamic> list, int index) {
    if (index < list.length) {
      return list[index];
    }
    return null;
  }

  static bool validateAndSave(GlobalKey<FormState> key) {
    final form = key.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  static bool isPhone() {
    final data = MediaQueryData.fromWindow(WidgetsBinding.instance.window);
    return data.size.shortestSide < 600;
  }

  static void showAlertDialog(
    BuildContext context, {
    String message,
    String title,
    Function onCancel,
    Function onOk,
    bool showCancel,
    String textCancel,
    String textOk,
    Widget child,
    bool barrierDismissible,
  }) {
    showDialog(
        context: context,
        barrierDismissible: barrierDismissible ?? false,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.0)), //this right here
            child: child ??
                AlertDialogContainer(
                  message: message,
                  label: title,
                  textCancel: textCancel,
                  textOk: textOk,
                  cancel: () {
                    Navigator.pop(context);
                    if (onCancel != null) {
                      onCancel();
                    }
                  },
                  confirm: () {
                    Navigator.pop(context);
                    if (onOk != null) {
                      onOk();
                    }
                  },
                  showCancel: showCancel,
                ),
          );
        });
  }

  static String formatTextSearch(String input) {
    return TiengViet.parse(input).toLowerCase();
  }

  static void dismissKeyBoard(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
  }

  static double getHeightKeyboard(BuildContext context) {
    final value = MediaQuery.of(context).viewInsets.bottom;
    if (value == 0) {
      return CommonConst.defaultHeightKeyBoard;
    }
    return value;
  }

  static double getBounceDegreesByKey(GlobalKey key) {
    final RenderBox box = key?.currentContext?.findRenderObject();
    final Offset position = box?.localToGlobal(Offset.zero);
    return position?.dy ?? 0;
  }

  static Future<bool> isConnected() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
      return false;
    } catch (_) {
      return false;
    }
  }

  static Future<Uint8List> getBytesFromAsset(String path, int size) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: size - 20, targetHeight: size);
    ui.FrameInfo fi = await codec.getNextFrame();
    final result = await fi.image.toByteData(format: ui.ImageByteFormat.png);
    return result.buffer.asUint8List();
  }

  static String textHelloInHome() {
    int hour = DateTime.now().hour;
    if (hour >= 4 && hour < 12) {
      return 'Chào buổi sáng';
    }
    if (hour == 12) {
      return 'Chào buổi trưa';
    }
    if (hour >= 13 && hour <= 18) {
      return 'Chào buổi chiều';
    }
    return 'Chào buổi tối';
  }

  static bool isEmptyOrNull(dynamic obj) {
    try {
      return (obj == null || obj.isEmpty);
    } catch (e) {
      return true;
    }
  }

  static bool isNotEmptyAndNull(dynamic obj) {
    try {
      return (obj != null && obj.isNotEmpty);
    } catch (e) {
      return false;
    }
  }

  static Gradient getGradient(
      {  List<Color> colors, GradientDirection gradientDirection}) {
    Alignment _getGradientDirection(String key) {
      final Map<String, Alignment> map = {
        'begin': Alignment.topCenter,
        'end': Alignment.bottomCenter
      };
      if (gradientDirection == null) {
        return map[key];
      }
      switch (gradientDirection) {
        case GradientDirection.ltr:
          map['begin'] = Alignment.centerLeft;
          map['end'] = Alignment.centerRight;
          break;
        case GradientDirection.rtl:
          map['begin'] = Alignment.centerRight;
          map['end'] = Alignment.centerLeft;
          break;
        case GradientDirection.ttb:
          map['begin'] = Alignment.topCenter;
          map['end'] = Alignment.bottomCenter;
          break;
        case GradientDirection.btt:
          map['begin'] = Alignment.bottomCenter;
          map['end'] = Alignment.topCenter;
          break;
      }
      return map[key];
    }

    return LinearGradient(
      colors: colors??AppColors.colorsGradient,
      begin: _getGradientDirection('begin'),
      end: _getGradientDirection('end'),
    );
  }
}
