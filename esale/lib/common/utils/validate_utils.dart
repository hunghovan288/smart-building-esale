import 'package:esale/common/extentions/string_extensions.dart';
class ValidateUtil {
  static const String textValidPrice =
      'Số tiền không hợp lệ hoặc không được để trống';

  static const String typePhoneNumber = 'Nhập số điện thoại đã bạn nhé';
  static const String inValidPhoneNumber =
      'Có vẻ như số điện thoại không hợp lệ';
  static const String inValidCurrency = 'Số tiền không hợp lệ';

  static String validName(String name) {
    if (name.length < 2) {
      return 'Hãy nhập họ tên đã bạn nhé';
    }
    return null;
  }

  static String validTitleRate(String input) {
    if (input.isEmpty) {
      return 'Hãy nhập tiêu đề đánh giá đã bạn nhé';
    }
    return null;
  }

  static String validContentRate(String input) {
    if (input.isEmpty) {
      return 'Nội dung đánh giá không được bỏ trống bạn nhé';
    }
    return null;
  }

  static String validReason(String input) {
    if (input.isEmpty) {
      return 'Vui lòng nhập lý do';
    }
    return null;
  }

  static String validAddress(String name) {
    if (name.isEmpty) {
      return 'Hãy chọn địa chỉ đã bạn nhé';
    }
    return null;
  }

  static String validStatusProduct(String input) {
    if (input.isEmpty) {
      return 'Hãy chọn tình trạng hàng đã bạn nhé';
    }
    return null;
  }

  static String validCategory(String name) {
    if (name.isEmpty) {
      return 'Hãy chọn danh mục cho sản phẩm đã bạn nhé';
    }
    return null;
  }

  static String validPhone(String phone, {bool forLogin = true}) {
    List<String> _phones = [
      '0123456781',
      '0123456782',
      '0123456783',
      '0123456784',
      '0123456785',
      '0123456786',
      '0123456787',
      '0123456788',
      '0123456789'
    ];
    if (_phones.contains(phone) && forLogin) {
      return null;
    }
    if (phone.isNotEmpty && phone[0] != '0') {
      phone = '0$phone';
    }
    const String patttern = r'(^(09|03|08|05|07)+([0-9]{8})\b$)';
    final RegExp regExp = RegExp(patttern);
    if (phone.isEmptyOrNull) {
      return typePhoneNumber;
    } else if (!regExp.hasMatch(phone)) {
      return inValidPhoneNumber;
    }
    return null;
  }

  static String validNameProduct(String name) {
    if (name.isEmpty) {
      return 'Tên sản phẩm không được để trống';
    }
    return null;
  }

  static String validNumberProduct(String name) {
    if (name.isEmpty) {
      return 'Nhập số lượng sản phẩm đã bạn nhé';
    }
    return null;
  }

  static String validDescriptionProduct(String name) {
    if (name.isEmpty) {
      return 'Mô tả sản phẩm không được để trống';
    }
    if (name.length < 20) {
      return 'Mô tả dài thêm tí nữa nhé, không được ít hơn 20 kí tự';
    }
    return null;
  }

  static String validEmail(String input) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

    RegExp regExp = new RegExp(p);
    if (regExp.hasMatch(input)) {
      return null;
    }
    return 'Email không hợp lệ';
  }
}
