class StringConst{
  static const String _path = 'StringConst.';
  static const String more = '${_path}search';
  static const String billion = '${_path}billion';
  static const String million = '${_path}million';
  static const String cost = '${_path}cost';
  static const String bathRoom = '${_path}bathRoom';
  static const String bedRoom = '${_path}bedRoom';
  static const String all = '${_path}all';
  static const String description = '${_path}description';
  static const String feature = '${_path}feature';
  static const String location = '${_path}location';
  static const String onSale = '${_path}onSale';
  static const String onDesign = '${_path}onDesign';
  static const String startGate = '${_path}startGate';
  static const String save = '${_path}save';

  static const String rules = '${_path}rules';
  static const String contract = '${_path}contract';
  static const String legalRecords = '${_path}legalRecords';
  static const String guarantee = '${_path}guarantee';
  static const String call = '${_path}call';
  static const String message = '${_path}message';
  static const String pay = '${_path}pay';
  static const String paid = '${_path}paid';
  static const String map = '${_path}map';
  static const String notification = '${_path}notification';
  static const String recently = '${_path}recently';
  static const String language = '${_path}language';
  static const String help = '${_path}help';
  static const String logout = '${_path}logout';
}