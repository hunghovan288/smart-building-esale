class IconConst {
  static const String path = 'assets/icons/';
  static const String imagePlaceHolder = '${path}image_place_holder.svg';
  static const String back = '${path}back.svg';
  static const String iconClose = '${path}cancel.svg';
  static const String arrowBack = '${path}arrow_back.svg';
  static const String share = '${path}share.svg';
  static const String note = '${path}note.svg';
  static const String question = '${path}question.svg';
  static const String right = '${path}arrow_right.svg';
  static const String arrowRight = '${path}arrow_right1.svg';
  static const String word = '${path}word.png';
  static const String pdf = '${path}pdf.png';
  static const String medal = '${path}medal.svg';
  static const String filterPng = '${path}filter.png';
  static const String mail = '${path}mail.svg';
  static const String call = '${path}call.svg';
  static const String camera = '${path}camera.svg';
  static const String password = '${path}password.svg';
  static const String bellPng = '${path}bell.png';
  static const String settings = '${path}settings.svg';

  /// login page
  static const String hidePass = '${path}login_hide_pass.svg';
  static const String showPass = '${path}login_show_password.svg';

  /// screen container
  static const String bottomSearch = '${path}bottom_search.svg';
  static const String bottomAccount = '${path}bottom_account.svg';
  static const String bottomChat = '${path}bottom_chat.svg';
  static const String bottomCustomer = '${path}bottom_customer.svg';

  /// search screen
  static const String searchProject = '${path}search_project.png';
  static const String searchHouse = '${path}search_house.png';
  static const String searchMap = '${path}search_map.png';
  static const String searchNews = '${path}search_news.png';
  static const String searchLocation = '${path}search_location.svg';


  /// search detail screen
  static const String searchDetailFilter = '${path}search_detail_filter.png';
  static const String searchDetailArea = '${path}search_detail_area.svg';
  static const String searchDetailBathRoom = '${path}search_detail_bathroom.svg';
  static const String searchDetailBedRoom = '${path}search_detail_bedroom.svg';
  static const String searchDetailCurrentLocation = '${path}search_detail_current_location.svg';


  /// house detail screen
  static const String house = '${path}search_detail_current_location.svg';

  /// detail house screen
  static const String houseBalcony = '${path}house_balcony.svg';
  static const String houseCalInstallment = '${path}house_cal_installment.svg';
  static const String houseCctv = '${path}house_cctv.svg';
  static const String houseFurniture = '${path}house_furniture.svg';
  static const String housePark = '${path}house_park.svg';
  static const String houseSecurity = '${path}house_security.svg';
  static const String houseSwimmingPool = '${path}house_swimming_pool.svg';

  /// detail project screen
  static const String projectContract = '${path}project_contract.svg';
  static const String projectGuarente = '${path}repeat.png';
  static const String projectLegalRecords = '${path}project_legal_gecords.svg';
  static const String projectRules = '${path}project_rules.svg';

  /// account screen
  static const String accountMap = '${path}account_map.svg';
  static const String accountNotification= '${path}account_notification.svg';
  static const String accountLast= '${path}clock.svg';
  static const String accountLanguage= '${path}language.svg';
  static const String accountLogout= '${path}logout.png';


}
