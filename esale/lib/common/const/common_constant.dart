import 'dart:math';

import 'package:flutter/material.dart';

class CommonConst {
  static const double defaultHeightKeyBoard = 175;

  static List<BoxShadow> defaultShadow = [
    BoxShadow(
        color: Colors.black.withOpacity(0.05), spreadRadius: 3, blurRadius: 5)
  ];

}
