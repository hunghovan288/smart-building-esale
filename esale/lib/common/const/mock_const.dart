import 'package:esale/data/models/house_model.dart';
import 'package:esale/data/models/message_model.dart';
import 'package:esale/data/models/person_model.dart';

class MockConst {
  static const String path = 'assets/mock/';
  static const String mockMap = '${path}mock_map.png';
  static const String mockMau1 = '${path}mock_mau1.png';
  static const String mockGiayPhep = '${path}mock_giay_phep.png';
  static const String mockMapSearch = '${path}map_map_search.png';

  static List<MessageModel> mockMessage = [
    MessageModel(
        url: urlPersons[0],
        message: 'Em gửi lại chỉ ảnh căn 2 ngủ nhé...',
        time: '1:24 PM',
        userName: 'Diệu Linh'),
    MessageModel(
        url: urlPersons[1],
        message: 'Anh cần gặp chú chiều nay nhé!!',
        time: '1:24 PM',
        userName: 'David'),
    MessageModel(
        url: urlPersons[2],
        message: 'Báo giá cho anh nhé em!',
        time: '1:24 PM',
        userName: 'Hoang Anh'),
    MessageModel(
        url: urlPersons[1],
        message: 'Báo giá cho anh nhé em!',
        time: '1:24 PM',
        userName: 'Anh Hoàng'),
  ];
  static List<PersonModel> mockPerson = [
    PersonModel(
        url: urlPersons[0],
        status: 'Tiềm năng',
        name: 'Hoàng Thùy Linh',
        numberMeet: 2),
    PersonModel(
        url: urlPersons[1],
        status: 'Rất thích',
        name: 'David Backham',
        numberMeet: 2),
    PersonModel(
        url: urlPersons[2],
        status: 'Quan tâm',
        name: 'Sơn Tùng MTP',
        numberMeet: 2),
  ];
  static List<String> urlPersons = [
    'http://media.baohaiduong.vn/files/library/images/site-1/20200307/web/dep-nhu-hoang-thuy-linh-mac-gi-cung-chat-32-173527.jpg',
    'https://i1-giaitri.vnecdn.net/2021/04/14/David-Beckham-5550-1618365663.jpg?w=680&h=0&q=100&dpr=1&fit=crop&s=7DI4HSmgr_-AHzNL4akXlw',
    'https://ss-images.saostar.vn/pc/1615898174397/saostar-t88yi8ypcw5ke7d8.jpg',
  ];
  static List<String> imageHouse = [
    'https://batdongsantamque.com/wp-content/uploads/2020/02/86175344_866025647166074_5125111000311267328_n.jpg',
    'https://media-cdn.laodong.vn/Storage/NewsPortal/2020/6/24/814790/A2-1.jpg?w=414&h=276&crop=auto&scale=both',
    'https://static2.cafeland.vn/static01/sgd/news/2020/02/ban-can-ho-chung-cu-long-bien-ha-noi-1582607513-nhadat.cafeland.vn.jpg',
    'https://alonhadat.com.vn/files/properties/2020/7/22/images/0quan-ly-cho-thue-chung-cu-florence-tran-huu-duc-da-dang-cac-can-ho-hay-lh-de-duoc-ep-gia.jpg'
  ];

  static List<HouseModel> mockHouses() {
    return [
      HouseModel(
          address: 'Phạm Văn Đồng HCM',
          listImage: imageHouse,
          cost: '3.8 Tỷ - 5 Tỷ',
          name: 'Gem Riverside',
          numberRoom: 3,
          background:
              'https://batdongsantamque.com/wp-content/uploads/2020/02/86175344_866025647166074_5125111000311267328_n.jpg',
          avatarPerson: urlPersons,
          type: 'Penthouse',
          area: '500 M',
          costPerMeter: '200T',
          description:
              'Căn hộ cao cấp bậc nhất khu Center Park, nằm tại vị trí đắc địa giữa trung tâm thành phố Hồ Chí Minh.',
          electrical: '2200 watt',
          floor: '31-32',
          furniture: 'Tân cổ điển',
          installment: '200T/M',
          numberToilet: 3,
          status: 'Đang bán',
          yearBuilt: '2021'),
      HouseModel(
          address: 'Phạm Văn Đồng HN',
          cost: '3.8 Tỷ - 5 Tỷ',
          name: 'Opal Boulevard',
          numberRoom: 2,
          background:
              'https://media-cdn.laodong.vn/Storage/NewsPortal/2020/6/24/814790/A2-1.jpg?w=414&h=276&crop=auto&scale=both',
          avatarPerson: urlPersons),
      HouseModel(
          address: 'Mỹ Đình HN',
          cost: '3.8 Tỷ - 5 Tỷ',
          name: 'Gem Riverside',
          numberRoom: 4,
          background:
              'https://static2.cafeland.vn/static01/sgd/news/2020/02/ban-can-ho-chung-cu-long-bien-ha-noi-1582607513-nhadat.cafeland.vn.jpg',
          avatarPerson: urlPersons),
      HouseModel(
          address: 'Trần Hữu Dực',
          cost: '3.8 Tỷ - 5 Tỷ',
          name: 'Vinhome Riverside',
          numberRoom: 5,
          background:
              'https://alonhadat.com.vn/files/properties/2020/7/22/images/0quan-ly-cho-thue-chung-cu-florence-tran-huu-duc-da-dang-cac-can-ho-hay-lh-de-duoc-ep-gia.jpg',
          avatarPerson: urlPersons)
    ];
  }
}
