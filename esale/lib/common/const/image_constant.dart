class ImageConst{
  static const String path = 'assets/images/';
  static const String splash_bg = '${path}splash_bg.png';
  static const String searchGuideSell = '${path}search_guide_sell.png';

}