class RouteName {
  static const String initial = '/';
  static const String splashScreen = 'splashScreen';
  static const String loginScreen = 'loginScreen';
  static const String screenContainer = 'ScreenContainer';
  static const String searchDetailScreen = 'SearchDetailScreen';
  static const String filterScreen = 'FilterScreen';
  static const String houseDetailScreen = 'HouseDetailScreen';
  static const String installmentScreen = 'installmentScreen';
  static const String mapScreen = 'MapScreen';
  static const String projectScreen = 'ProjectScreen';
  static const String projectDetailScreen = 'ProjectDetailScreen';
  static const String rulesScreen = 'RulesScreen';
  static const String legalRecordsScreen = 'LegalRecordsScreen';
  static const String documentDetailScreen = 'DocumentDetailScreen';
  static const String documentScreen = 'DocumentScreen';
  static const String detailLicenseScreen = 'DetailLicenseScreen';
  static const String contractScreen = 'ContractScreen';
  static const String userDetailScreen = 'UserDetailScreen';
  static const String payContractScreen = 'PayContractScreen';
  static const String editUserScreen = 'EditUserScreen';
  static const String notificationScreen = 'NotificationScreen';
  static const String newsScreen = 'NewsScreen';
}
