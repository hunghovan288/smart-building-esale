import 'package:esale/common/const/image_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    _mockGetData();
    super.initState();
  }

  void _mockGetData() async {
    Future.delayed(Duration(seconds: 2))
        .then((value) => Routes.instance.navigateTo(RouteName.loginScreen));
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Image.asset(
        ImageConst.splash_bg,
        width: double.infinity,
        height: double.infinity,
        fit: BoxFit.cover,
      ),
    );
  }
}
