import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/auth/login/login_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_common_button.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/common/extentions/screen_extension.dart';
import 'package:esale/presentation/widgets/custom_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _showPass = false;

  void _onChangeStatusPass() {
    setState(() {
      _showPass = !_showPass;
    });
  }

  void _onForgotPassword() {
    LOG.d('_onForgotPassword');
  }
  void _onComeToSeller() {
    LOG.d('_onComeToSeller');
  }
  void _onLogin(){
    Routes.instance.navigateAndRemove(RouteName.screenContainer);
  }
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      resizeToAvoidBottomInset: false,
      autoDismissKeyboard: true,
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 28.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 158.h),
            Text(
              translate(LoginConst.loginToContinue),
              style: AppTextTheme.w700_14px.copyWith(fontSize: 24),
            ),
            SizedBox(height: 70.h),
            Text(
              translate(LoginConst.userName),
              style: AppTextTheme.w500_14px,
            ),
            CustomTextField(
              hintText: translate(LoginConst.typeUserName),
              contentPaddingLeft: 0,
              style: AppTextTheme.w500_16px,
            ),
            SizedBox(height: 32.h),
            Text(
              translate(LoginConst.passWord),
              style: AppTextTheme.w500_14px,
            ),
            CustomTextField(
              hintText: translate(LoginConst.typePassword),
              contentPaddingLeft: 0,
              style: AppTextTheme.w500_16px,
              isPassword: !_showPass,
              suffixIcon: InkWell(
                onTap: _onChangeStatusPass,
                child: SvgPicture.asset(
                  _showPass ? IconConst.hidePass : IconConst.showPass,
                  width: 30,
                  height: 30,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SizedBox(height: 30.h),
            CustomCommonButton(
              text: translate(LoginConst.login),
              onTap: _onLogin,
            ),
            SizedBox(height: 6),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: _onForgotPassword,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 16, top: 14, bottom: 14),
                    child: Text(
                      translate(LoginConst.forgotPassword),
                      style: AppTextTheme.w500_14px,
                    ),
                  ),
                )
              ],
            ),
            const Spacer(),
            InkWell(
              onTap: _onComeToSeller,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      translate(LoginConst.comeSeller),
                      style: AppTextTheme.w400_12px,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      translate(LoginConst.comeSeller),
                      style: AppTextTheme.w700_12px
                          .copyWith(color: AppColors.primaryColor),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: ScreenUtil.bottomBarHeight + 32,
            )
          ],
        ),
      ),
    );
  }
}
