class LoginConst{
  static const String _path = 'LoginConst.';
  static const String loginToContinue = '${_path}loginToContinue';
  static const String userName = '${_path}userName';
  static const String passWord = '${_path}passWord';
  static const String typeUserName = '${_path}typeUserName';
  static const String typePassword = '${_path}typePassword';
  static const String login = '${_path}login';
  static const String forgotPassword = '${_path}forgotPassword';
  static const String comeSeller = '${_path}comeSeller';
  static const String signUpHere = '${_path}signUpHere';
}