import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchLayoutWhatYouNeed extends StatelessWidget {
  void _onProject() {
    Routes.instance.navigateTo(RouteName.projectScreen);
  }

  void _onMap() {
    Routes.instance.navigateTo(RouteName.mapScreen);
  }

  void _onHouse() {
    Routes.instance.navigateTo(RouteName.searchDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SearchIconNeed(
            icon: IconConst.searchProject,
            text: translate(SearchConst.project),
            onTap: _onProject,
          ),
          SearchIconNeed(
            icon: IconConst.searchMap,
            onTap: _onMap,
            text: translate(SearchConst.map),
          ),
          SearchIconNeed(
            icon: IconConst.searchNews,
            text: translate(SearchConst.news),
            onTap: () {
              Routes.instance.navigateTo(RouteName.newsScreen);
            },
          ),
          SearchIconNeed(
            icon: IconConst.searchHouse,
            text: translate(SearchConst.house),
            onTap: _onHouse,
          ),
        ],
      ),
    );
  }
}

class SearchIconNeed extends StatelessWidget {
  final String icon;
  final String text;
  final Function onTap;

  const SearchIconNeed({Key key, this.icon, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        child: Column(
          children: [
            Container(
              width: 60,
              height: 60,
              margin: EdgeInsets.only(bottom: 8),
              decoration: BoxDecoration(
                  color: AppColors.grey3,
                  borderRadius: BorderRadius.circular(20)),
              child: Center(
                child: Image.asset(
                  icon,
                  width: 32,
                  height: 32,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Text(
              text ?? '',
              style: AppTextTheme.w500_16px.copyWith(fontSize: 10),
            ),
          ],
        ),
      ),
    );
  }
}
