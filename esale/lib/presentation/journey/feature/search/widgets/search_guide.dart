import 'package:esale/common/const/image_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchGuide extends StatelessWidget {
  void _onWatchNow() {
    LOG.d('_onWatchNow');
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal),
      child: Row(
        children: [
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 8),
                Text(
                  translate(SearchConst.introductionSell),
                  style: AppTextTheme.w700_16px,
                ),
                const SizedBox(height: 12),
                Text(
                  translate(SearchConst.introductDes),
                  style: AppTextTheme.w400_14px,
                ),
                InkWell(
                  onTap: _onWatchNow,
                  child: Padding(
                    padding:
                        const EdgeInsets.only(top: 12, right: 12, bottom: 12),
                    child: Container(
                      width: 106,
                      height: 32,
                      decoration: BoxDecoration(
                          gradient: CommonUtil.getGradient(
                              colors: AppColors.colorsGradient),
                          borderRadius: BorderRadius.circular(4)),
                      child: Center(
                        child: Text(
                          translate(SearchConst.watchNow),
                          style: AppTextTheme.w400_14px
                              .copyWith(fontSize: 12, color: AppColors.white),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Image.asset(
                    ImageConst.searchGuideSell,
                    width: double.infinity,
                    height: 60,
                  )
                ],
              ))
        ],
      ),
    );
  }
}
