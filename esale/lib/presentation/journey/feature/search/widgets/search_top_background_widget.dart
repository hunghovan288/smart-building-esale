import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:esale/common/extentions/screen_extension.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchTopBackgroundWidget extends StatelessWidget {
   void _onTap(){
     Routes.instance.navigateTo(RouteName.searchDetailScreen);
   }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onTap,
      child: Container(
        height: 252.h,
        padding: EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
            gradient: CommonUtil.getGradient(
              colors: [
                AppColors.primaryLight,
                AppColors.primaryDart,
              ],
            )),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Text(
              translate(SearchConst.everyHouse),
              style: AppTextTheme.w500_16px
                  .copyWith(color: AppColors.white, fontSize: 20),
            ),
            const SizedBox(height: 12),
            Text(
              translate(SearchConst.sellAndRent),
              style: AppTextTheme.w700_12px
                  .copyWith(color: AppColors.white, fontSize: 24),
            ),
            Container(
              height: 44,
              margin: EdgeInsets.symmetric( vertical: 32),
              decoration: BoxDecoration(
                  color: AppColors.white, borderRadius: BorderRadius.circular(5)),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: SvgPicture.asset(
                      IconConst.bottomSearch,
                      color: AppColors.grey7,
                      width: 20,
                      height: 20,
                    ),
                  ),
                  Text(
                    translate(SearchConst.searchHouse),
                    style: AppTextTheme.w400_14px.copyWith(color: AppColors.grey7),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
