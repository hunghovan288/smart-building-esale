import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/journey/feature/search/widgets/search_guide.dart';
import 'package:esale/presentation/journey/feature/search/widgets/search_you_need.dart';
import 'package:esale/presentation/journey/feature/search/widgets/search_top_background_widget.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/item_house.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SearchTopBackgroundWidget(),
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal, vertical: 24),
              child: Text(
                translate(SearchConst.whatYouNeed),
                style: AppTextTheme.w500_16px.copyWith(fontSize: 18),
              ),
            ),
            SearchLayoutWhatYouNeed(),
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal, vertical: 20),
              child: Text(
                translate(SearchConst.houseHighLight),
                style: AppTextTheme.w600_16px.copyWith(fontSize: 18),
              ),
            ),

            Container(
              height: 320,
              padding: EdgeInsets.only(bottom: 5),
              width: double.infinity,
              child: ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
                  itemBuilder: (_, index) => ItemHouse(
                        houseModel: MockConst.mockHouses()[index],
                      ),
                  separatorBuilder: (_, i) => const SizedBox(width: 16),
                  scrollDirection: Axis.horizontal,
                  itemCount: MockConst.mockHouses().length),
            ),
            SearchGuide(),
          ],
        ),
      ),
    );
  }
}
