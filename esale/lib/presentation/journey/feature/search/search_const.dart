class SearchConst{
  static const String _path = 'SearchConst.';
  static const String everyHouse = '${_path}everyHouse';
  static const String sellAndRent = '${_path}sellAndRent';
  static const String searchHouse = '${_path}searchHouse';
  static const String whatYouNeed = '${_path}whatYouNeed';
  static const String project = '${_path}project';
  static const String map = '${_path}map';
  static const String news = '${_path}news';
  static const String house = '${_path}house';
  static const String houseHighLight = '${_path}houseHighLight';
  static const String billion = '${_path}billion';
  static const String bedroom = '${_path}bedroom';
  static const String introductionSell = '${_path}introductionSell';
  static const String introductDes = '${_path}introductDes';
  static const String watchNow = '${_path}watchNow';
}