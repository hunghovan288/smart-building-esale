import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/journey/feature/user_detail/pay_contract/widget/custom_circle_progress.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class PayContractPercent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      height: 108,
      decoration: BoxDecoration(
        gradient: CommonUtil.getGradient(colors: [
          Color(0xffFEC031).withOpacity(0.0),
          Color(0xffFEC031).withOpacity(0.15),
        ], gradientDirection: GradientDirection.ltr),
      ),
      child: Row(
        children: [
          CustomCirclePercent(
            percent: 64,
            color: AppColors.yellow,
            backgroundColor: AppColors.grey4,
            size: 40,
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  translate(StringConst.pay),
                  style: AppTextTheme.w700_16px,
                ),
                const SizedBox(height: 4),
                Text(
                  'Tiến độ thanh toán theo khoản tiền đã nhận của khách hàng và các bước của hợp đồng.',
                  maxLines: 2,
                  style: AppTextTheme.w400_12px,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
