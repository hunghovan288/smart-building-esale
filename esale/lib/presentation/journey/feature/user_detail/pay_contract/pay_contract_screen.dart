import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/user_detail/pay_contract/widget/pay_contract_percent.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/stype_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class PayContractScreen extends StatefulWidget {
  @override
  _PayContractScreenState createState() => _PayContractScreenState();
}

class _PayContractScreenState extends State<PayContractScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(StringConst.contract),
      ),
      body: Column(
        children: [
          PayContractPercent(),
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal, vertical: 25),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      translate(StringConst.cost),
                      style: AppTextTheme.w500_16px,
                    ),
                    const Spacer(),
                    Text(
                      '2,300,000,000 vnđ',
                      style: AppTextTheme.w500_16px,
                    )
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  children: [
                    Text(
                      translate(StringConst.paid),
                      style: AppTextTheme.w500_16px
                          .copyWith(color: AppColors.primaryColor),
                    ),
                    const Spacer(),
                    Text(
                      '1,472,000,000 vnđ',
                      style: AppTextTheme.w500_16px
                          .copyWith(color: AppColors.primaryColor),
                    )
                  ],
                ),
              ],
            ),
          ),
          const Spacer(),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal,
                vertical: 20 + ScreenUtil.bottomBarHeight),
            child: Row(
              children: [
                Expanded(
                    flex: 2,
                    child: _iconButton(
                      text: StringConst.call,
                      icon: IconConst.call,
                      colors: [
                        Color(0xff00AB6B).withOpacity(0.8),
                        Color(0xff00AB6B),
                      ],
                    )),
                const SizedBox(width: 20),
                Expanded(
                    flex: 5,
                    child: _iconButton(
                      text: StringConst.message,
                      icon: IconConst.bottomChat,
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _iconButton({String text, String icon, List<Color> colors}) =>
      Container(
        height: 44,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: CommonUtil.getGradient(colors: colors),
            borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon,
              width: 20,
              height: 20,
              color: AppColors.white,
            ),
            const SizedBox(width: 6),
            Text(
              translate(text) ?? '',
              style: AppTextTheme.w400_14px.copyWith(color: AppColors.white),
            )
          ],
        ),
      );
}
