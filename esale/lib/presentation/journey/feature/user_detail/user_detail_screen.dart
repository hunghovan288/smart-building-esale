import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/journey/feature/user_detail/widgets/user_detail_avatar.dart';
import 'package:esale/presentation/journey/feature/user_detail/widgets/user_detail_contract.dart';
import 'package:esale/presentation/journey/feature/user_detail/widgets/user_detail_info.dart';
import 'package:esale/presentation/journey/feature/user_detail/widgets/user_detail_introductioned.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class UserDetailScreen extends StatefulWidget {
  @override
  _UserDetailScreenState createState() => _UserDetailScreenState();
}

class _UserDetailScreenState extends State<UserDetailScreen> {
  void _onMore() {
    LOG.d('_onMore');
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        widgetRight: InkWell(
          onTap: _onMore,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
            child: Icon(Icons.more_vert),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal),
              child: Column(
                children: [
                  const SizedBox(height: 12),
                  UserDetailAvatar(),
                  UserDetailInfo(),
                  UserDetailContract(),
                  const SizedBox(height: 20),
                ],
              ),
            ),
            Container(
              width: double.infinity,
              height: 7,
              color: AppColors.grey3,
            ),
            UserDetailIntroduced(),
          ],
        ),
      ),
    );
  }
}
