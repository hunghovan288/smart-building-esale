import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/journey/feature/user_detail/user_detail_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';
import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UserDetailContract extends StatelessWidget {

  void _onTap() {
    Routes.instance.navigateTo(RouteName.payContractScreen);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, bottom: 5),
          child: Text(
            translate(UserDetailConst.signedContract),
            style: AppTextTheme.w700_16px,
          ),
        ),
        InkWell(
          onTap: _onTap,
          child: _ItemContract(
            houseModel: MockConst.mockHouses()[0],
          ),
        ),
      ],
    );
  }
}

class _ItemContract extends StatelessWidget {
  final HouseModel houseModel;
  final ProjectStatus projectStatus;

  const _ItemContract({Key key, this.houseModel, this.projectStatus})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CustomImageNetwork(
                url: houseModel.background,
                height: 105,
                width: 135,
                fit: BoxFit.cover,
                borderRadius: BorderRadius.circular(8),
              ),
              const SizedBox(width: 12),
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    decoration: BoxDecoration(
                        color: AppColors.yellow.withOpacity(0.15),
                        borderRadius: BorderRadius.circular(4)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 8),
                      child: Text(
                        'Đang thanh toán',
                        style: AppTextTheme.w400_12px
                            .copyWith(color: AppColors.yellow, fontSize: 12),
                      ),
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    'Căn hộ 3PN - ${houseModel.name}',
                    style: AppTextTheme.w700_14px,
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: [
                      SvgPicture.asset(
                        IconConst.searchLocation,
                        width: 14,
                        height: 14,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        houseModel.address ?? '',
                        style: AppTextTheme.w400_12px,
                      )
                    ],
                  ),
                  const SizedBox(height: 8),
                  Text(
                    '2,300,000,000 vnđ',
                    style: AppTextTheme.w600_14px,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
              ))
            ],
          ),
          const SizedBox(height: 20),
          Row(
            children: [
              Expanded(
                  flex: 2,
                  child: _iconButton(
                    text: StringConst.call,
                    icon: IconConst.call,
                    colors: [
                      Color(0xff00AB6B).withOpacity(0.8),
                      Color(0xff00AB6B),
                    ],
                  )),
              const SizedBox(width: 20),
              Expanded(
                  flex: 5,
                  child: _iconButton(
                    text: StringConst.message,
                    icon: IconConst.bottomChat,
                  ))
            ],
          )
        ],
      ),
    );
  }

  Widget _iconButton({String text, String icon, List<Color> colors}) =>
      Container(
        height: 44,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: CommonUtil.getGradient(colors: colors),
            borderRadius: BorderRadius.circular(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon,
              width: 20,
              height: 20,
              color: AppColors.white,
            ),
            const SizedBox(width: 6),
            Text(
              translate(text) ?? '',
              style: AppTextTheme.w400_14px.copyWith(color: AppColors.white),
            )
          ],
        ),
      );
}
