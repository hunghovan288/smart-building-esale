import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UserDetailAvatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Stack(
          children: [
            Column(
              children: [
                CustomImageNetwork(
                  url: MockConst.urlPersons[1],
                  width: 52,
                  height: 52,
                  border: 26,
                  fit: BoxFit.cover,
                ),
                const SizedBox(height: 10),
              ],
            ),
            Positioned.fill(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: SvgPicture.asset(
                  IconConst.medal,
                  width: 16,
                  height: 20,
                ),
              ),
            )
          ],
        ),
        const SizedBox(width: 16),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Hoàng Thùy Linh',
                style: AppTextTheme.w500_16px,
              ),
              const SizedBox(height: 6),
              Text(
                'Tiềm năng',
                style: AppTextTheme.w400_14px,
              )
            ],
          ),
        ),

      ],
    );
  }
}
