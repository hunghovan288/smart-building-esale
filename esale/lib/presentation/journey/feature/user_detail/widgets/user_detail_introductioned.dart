import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/user_detail/user_detail_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:flutter/material.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class UserDetailIntroduced extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 20,horizontal: PixelConstant.marginHorizontal),
          child: Text(
            translate(UserDetailConst.introduced),
            style: AppTextTheme.w700_16px,
          ),
        ),
        InkWell(
          onTap: (){
            Routes.instance.navigateTo(RouteName.houseDetailScreen);
          },
          child: _Item(

            houseModel: MockConst.mockHouses()[0],
          ),
        ),
        const SizedBox(height: 40),
      ],
    );
  }
}

class _Item extends StatelessWidget {
  final HouseModel houseModel;

  const _Item({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Container(
        width: ScreenUtil.screenWidthDp,
        margin:
            EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: AppColors.grey4, width: 1)),
        height: 145,
        child: Row(
          children: [
            CustomImageNetwork(
              url: houseModel.background,
              width: 145,
              height: 145,
              border: 8,
              fit: BoxFit.cover,
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.all(12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 100,
                    height: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        gradient: CommonUtil.getGradient(
                            colors: AppColors.colorsGradient)),
                    child: Center(
                      child: Text(
                        houseModel.cost,
                        style: AppTextTheme.w500_14px
                            .copyWith(color: AppColors.white),
                      ),
                    ),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    'Chung Cu 2PN - Ocean Park',
                    style: AppTextTheme.w500_12px.copyWith(fontSize: 13),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(height: 12),
                  Row(
                    children: [
                      SvgPicture.asset(
                        IconConst.searchLocation,
                        width: 14,
                        height: 14,
                        color: AppColors.grey,
                      ),
                      const SizedBox(width: 4),
                      Expanded(
                        child: Text(
                          houseModel.address,
                          style: AppTextTheme.w400_12px
                              .copyWith(color: AppColors.grey),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 12),
                  Row(
                    children: [
                      SvgPicture.asset(
                        IconConst.searchDetailBedRoom,
                        width: 20,
                        height: 20,
                      ),
                      Text(
                        '3',
                        style: AppTextTheme.w500_14px
                            .copyWith(color: AppColors.grey7, fontSize: 12),
                      ),
                      const SizedBox(width: 16),
                      SvgPicture.asset(
                        IconConst.searchDetailBathRoom,
                        width: 20,
                        height: 20,
                      ),
                      Text(
                        '2',
                        style: AppTextTheme.w500_14px
                            .copyWith(color: AppColors.grey7, fontSize: 12),
                      ),
                      const SizedBox(width: 16),
                      SvgPicture.asset(
                        IconConst.searchDetailArea,
                        width: 20,
                        height: 20,
                      ),
                      Text(
                        '170 M',
                        style: AppTextTheme.w500_14px
                            .copyWith(color: AppColors.grey7, fontSize: 12),
                      ),
                    ],
                  )
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
