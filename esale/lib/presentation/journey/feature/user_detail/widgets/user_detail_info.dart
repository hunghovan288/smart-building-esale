import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/presentation/journey/feature/user_detail/user_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class UserDetailInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top:30,bottom: 5),
          child: Text(
            translate(UserDetailConst.customerInfo),
            style: AppTextTheme.w700_16px,
          ),
        ),
        _item(IconConst.mail, 'LinhND37@gmail.com'),
        _item(IconConst.searchLocation, 'Hà Nội, Việt Nam'),
        _item(IconConst.searchLocation, 'Quan tâm tới chung cư Opal Boulevard'),
      ],
    );
  }

  Widget _item(String icon, String text) => Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 14),
            child: Row(
              children: [
                SvgPicture.asset(
                  icon,
                  width: 20,
                  height: 20,
                ),
                const SizedBox(width: 16),
                Text(
                  text ?? '',
                  style: AppTextTheme.w400_14px,
                ),
              ],
            ),
          ),
          Divider(
            height: 1,
            color: AppColors.grey5,
          ),
        ],
      );
}
