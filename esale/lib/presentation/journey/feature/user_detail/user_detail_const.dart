class UserDetailConst {
  static const String _path = 'UserDetailConst.';
  static const String customerInfo = '${_path}customerInfo';
  static const String signedContract = '${_path}signedContract';
  static const String introduced = '${_path}introduced';

}
