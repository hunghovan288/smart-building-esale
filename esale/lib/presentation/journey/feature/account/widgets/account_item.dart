import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class AccountItem extends StatelessWidget {
  final String icon;
  final String text;
  final Function onTap;
  final Widget childText;

  const AccountItem({Key key, this.icon, this.text, this.onTap, this.childText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        height: 56,
        child: Row(
          children: [
            icon.contains('png')
                ? Image.asset(
                    icon,
                    width: 24,
                    height: 24,
                  )
                : SvgPicture.asset(
                    icon,
                    width: 24,
                    height: 24,
                  ),
            const SizedBox(width: 16),
            childText ??
                Text(
                  translate(text) ?? '',
                  style: AppTextTheme.w500_16px,
                ),
          ],
        ),
      ),
    );
  }
}
