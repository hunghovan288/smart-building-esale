import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/auth/login/login_const.dart';
import 'package:esale/presentation/journey/feature/account/edit_user/edit_user_const.dart';
import 'package:esale/presentation/journey/feature/account/edit_user/widgets/edit_user_textfield.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class EditUserScreen extends StatefulWidget {
  @override
  _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
  ScrollController _scrollController;
  TextEditingController _userNameController;
  TextEditingController _emailController;
  TextEditingController _phoneController;
  TextEditingController _passwordController;
  TextEditingController _addressController;
  FocusNode _userNameFocusNode;
  FocusNode _emailFocusNode;
  FocusNode _phoneFocusNode;
  FocusNode _passwordFocusNode;
  FocusNode _addressFocusNode;

  @override
  void initState() {
    _userNameController = TextEditingController();
    _emailController = TextEditingController();
    _phoneController = TextEditingController();
    _passwordController = TextEditingController();
    _addressController = TextEditingController();
    _scrollController = ScrollController();
    _userNameFocusNode = FocusNode();
    _emailFocusNode = FocusNode();
    _phoneFocusNode = FocusNode();
    _passwordFocusNode = FocusNode();
    _addressFocusNode = FocusNode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: true,
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(EditUserConst.edit),
        widgetRight: InkWell(
          onTap: () {
            Routes.instance.pop();
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal, vertical: 12),
            child: Text(
              translate(StringConst.save),
              style: AppTextTheme.w700_14px
                  .copyWith(color: AppColors.primaryColor),
            ),
          ),
        ),
      ),
      body: ListView(
        controller: _scrollController,
        padding:
            EdgeInsets.only(top: 0, bottom: 20 + ScreenUtil.bottomBarHeight),
        children: [
          const SizedBox(height: 42),
          Container(
            width: double.infinity,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Stack(
                  children: [
                    CustomImageNetwork(
                      url: MockConst.urlPersons[1],
                      width: 120,
                      height: 120,
                      border: 60,
                      fit: BoxFit.cover,
                    ),
                    Positioned(
                      bottom: 0,
                      right: 0,
                      child: Container(
                        width: 40,
                        height: 40,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: AppColors.white,
                            boxShadow: CommonConst.defaultShadow),
                        child: Center(
                          child: SvgPicture.asset(
                            IconConst.camera,
                            width: 20,
                            height: 20,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 60),
          EditUserTextField(
            icon: IconConst.bottomAccount,
            label: EditUserConst.userName,
            controller: _userNameController,
            focusNode: _userNameFocusNode,
          ),
          EditUserTextField(
            icon: IconConst.mail,
            focusNode: _emailFocusNode,
            label: EditUserConst.email,
            controller: _emailController,
          ),
          EditUserTextField(
            icon: IconConst.call,
            label: EditUserConst.phone,
            controller: _phoneController,
            focusNode: _phoneFocusNode,
          ),
          EditUserTextField(
            icon: IconConst.password,
            label: LoginConst.passWord,
            controller: _passwordController,
            focusNode: _passwordFocusNode,
          ),
          EditUserTextField(
            icon: IconConst.searchLocation,
            label: EditUserConst.address,
            controller: _addressController,
            focusNode: _addressFocusNode,
          ),
        ],
      ),
    );
  }
}
