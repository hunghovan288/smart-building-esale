import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class EditUserTextField extends StatelessWidget {
  final String label;
  final String icon;
  final FocusNode focusNode;
  final TextEditingController controller;

  const EditUserTextField(
      {Key key, this.label, this.icon, this.controller, this.focusNode})
      : super(key: key);

  void _onFocus(BuildContext context) {
    FocusScope.of(context).requestFocus(focusNode);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _onFocus(context);
      },
      child: Container(
        width: double.infinity,
        height: 60,
        padding: EdgeInsets.symmetric(horizontal: 12),
        margin: EdgeInsets.symmetric(
            horizontal: PixelConstant.marginHorizontal, vertical: 12),
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.grey4, width: 1),
            borderRadius: BorderRadius.circular(5)),
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(height: 10),
                  Text(
                    translate(label),
                    style: AppTextTheme.w400_12px,
                  ),
                  Container(
                    width: double.infinity,
                    height: 30,
                    child: TextFormField(
                      style: AppTextTheme.w500_14px,
                      cursorColor: AppColors.primaryColor,
                      focusNode: focusNode,
                      decoration: InputDecoration(
                          hintText: ' ... ',
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: AppTextTheme.w400_14px
                              .copyWith(color: AppColors.grey7),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(bottom: 14)),
                    ),
                  )
                ],
              ),
            ),
            SvgPicture.asset(
              icon,
              width: 20,
              height: 20,
              color: AppColors.black,
            )
          ],
        ),
      ),
    );
  }
}
