class EditUserConst{
  static const String _path ='EditUserConst.';
  static const String edit ='${_path}edit';
  static const String userName ='${_path}userName';
  static const String email ='Email';
  static const String phone ='${_path}phone';
  static const String address ='${_path}address';
}