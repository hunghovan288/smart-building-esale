import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/presentation/journey/feature/account/notification/widgets/notification_item.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(StringConst.notification),
        iconRight: IconConst.settings,
      ),
      body: ListView(
        padding: EdgeInsets.only(top: 12),
        children: [
          NotificationItem(
            text: 'Bạn có 1 tin nhắn mới từ Diệu Linh,\nBấm vào đây để xem',
          ),
          NotificationItem(
            text: 'Anh Hoàng, Vũ Tuấn và 2 người khác đã gửi tin nhắn cho bạn.',
          ),
        ],
      ),
    );
  }
}
