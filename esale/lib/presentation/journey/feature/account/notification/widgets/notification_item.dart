import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class NotificationItem extends StatelessWidget {
  final String text;

  const NotificationItem({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 80,
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      child: Row(
        children: [
          Container(
            width: 32,
            height: 32,
            decoration: BoxDecoration(
                color: Color(0xffF4F5FB),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Image.asset(
                IconConst.bellPng,
                width: 16,
                height: 16,
              ),
            ),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        text ?? '',
                        style: AppTextTheme.w400_14px,
                      )
                    ],
                  ),
                ),
                Divider(
                  height: 1,
                  color: AppColors.grey5,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
