import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/account/widgets/account_item.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  void _onEditProfile() {
    Routes.instance.navigateTo(RouteName.editUserScreen);

  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Column(
        children: [
          SizedBox(
            height: 40 + ScreenUtil.statusBarHeight,
          ),
          InkWell(
            onTap: _onEditProfile,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CustomImageNetwork(
                  url: MockConst.urlPersons[0],
                  width: 80,
                  height: 80,
                  border: 40,
                  fit: BoxFit.cover,
                ),
                const SizedBox(height: 28),
                Text(
                  'Hoàng Thùy Linh',
                  style: AppTextTheme.w700_16px.copyWith(fontSize: 20),
                ),
                const SizedBox(height: 8),
                Text(
                  'rikie4374@gmail.com',
                  style: AppTextTheme.w400_14px,
                ),
              ],
            ),
          ),
          const SizedBox(height: 40),
          Divider(
            height: 1,
            indent: 30,
            endIndent: 30,
          ),
          const SizedBox(height: 20),
          AccountItem(
            icon: IconConst.accountMap,
            text: StringConst.map,
            onTap: () {
              Routes.instance.navigateTo(RouteName.mapScreen);
            },
          ),
          AccountItem(
            icon: IconConst.accountNotification,
            text: StringConst.notification,
            onTap: () {
              Routes.instance.navigateTo(RouteName.notificationScreen);
            },
          ),
          AccountItem(
            icon: IconConst.accountLast,
            text: StringConst.recently,
          ),
          AccountItem(
            icon: IconConst.accountLanguage,
            text: StringConst.language,
          ),
          AccountItem(
            icon: IconConst.question,
            text: StringConst.help,
          ),
          InkWell(
            onTap: () {
              Routes.instance.navigateAndRemove(RouteName.loginScreen);
            },
            child: AccountItem(
              icon: IconConst.accountLogout,
              childText: Text(
                translate(StringConst.logout),
                style: AppTextTheme.w400_14px.copyWith(color: AppColors.pink),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
