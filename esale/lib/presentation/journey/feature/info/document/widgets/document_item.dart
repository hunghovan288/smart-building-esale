import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class DocumentItem extends StatelessWidget {
  final String text;
  final String icon;
  final Function onTap;

  const DocumentItem({Key key, this.text, this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 74,
        margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(12),
          boxShadow: CommonConst.defaultShadow
        ),
        padding: EdgeInsets.all(12),
        child: Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image.asset(
                icon,
                width: 43,
                height: 50,
              ),
            ),
            const SizedBox(width: 12),
            Expanded(
              child: Text(
                text,
                style: AppTextTheme.w700_16px,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      ),
    );
  }
}
