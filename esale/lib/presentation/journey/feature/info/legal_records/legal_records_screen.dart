import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/journey/feature/info/document/widgets/document_item.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class LegalRecordsScreen extends StatefulWidget {
  @override
  _LegalRecordsScreenState createState() => _LegalRecordsScreenState();
}

class _LegalRecordsScreenState extends State<LegalRecordsScreen> {
  void _onItemTap() {
    Routes.instance.navigateTo(RouteName.documentDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(StringConst.legalRecords),
        iconRight: IconConst.question,
      ),
      body: ListView(
        children: [
          DocumentItem(
            icon: IconConst.pdf,
            text: 'Giấy phép xây dựng',
            onTap: _onItemTap,
          ),
          const SizedBox(height: 16),
          DocumentItem(
            icon: IconConst.word,
            text: 'Quyền sử dụng đất',
            onTap: _onItemTap,
          ),
          const SizedBox(height: 16),
          DocumentItem(
              icon: IconConst.pdf,
              text: 'Thiết kế và bản vẽ công trình',
              onTap: _onItemTap),
          const SizedBox(height: 16),
          DocumentItem(
              icon: IconConst.pdf,
              text: 'Quyết định đầu tư dự án',
              onTap: _onItemTap),
        ],
      ),
    );
  }
}
