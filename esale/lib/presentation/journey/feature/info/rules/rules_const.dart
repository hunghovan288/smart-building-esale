class RulesConst{
  static const String mock ='''1. Nghiêm cấm chiếm dụng nhà ở trái pháp luật, tự ý thay đổi kết cấu chịu lực, kiến trúc, điện nước hoặc thay đổi kết cấu căn hộ trong tòa nhà.

2. Nghiêm cấm cơi nới làm lồng sắt, mái tôn, mái vẩy khu vực ban công dưới mọi hình thức.

3. Nghiêm cấm tháo dỡ hoặc di dời làm hư hỏng các thiết bị kỹ thuật đã lắp đặt tại tòa nhà.

4. Nghiêm cấm quảng cáo, viết, vẽ trái quy định phía mặt ngoài căn hộ, khu công cộng.

5. Nghiêm cấm mang chất độc hại dễ cháy nổ như xăng, dầu, bếp than, vũ khí, đạn dược, chất cấm… vào tòa nhà.

6. Không vứt rác, phóng uế tại khu hành lang, ban công, thang máy, thang bộ. (Yêu cầu tự vận chuyển rác thải sinh hoạt đến khu vực tập kết đã quy định).

7. Không hóa vàng (đốt giấy tiền, đốt mã…) trong căn hộ, hành lang thuộc tòa nhà. (Yêu cầu tập kết hóa vàng. đốt mã đúng nơi quy định).

8. Không bỏ vào toilet, hố ga những đồ vật khó phân hủy hoặc làm tắc nghẽn các đường ống thoát chung.

9. Giữ gìn trật tự nơi công cộng, không gây tiếng ồn quá mức làm ảnh hưởng đến sinh hoạt của cư dân sinh sống trong tòa nhà.

10. Đỗ xe đúng nơi quy định. (Yêu cầu đăng ký gửi xe, đăng ký vé tháng với Ban quản lý tòa nhà).

11. Khách đến tòa nhà phải có trách nhiệm đăng ký với Ban quản lý và làm thủ tục tạm trú, tạm vắng theo quy định của Pháp luật.

12. Mọi cá nhân, tổ chức sinh sống và làm việc tại toà nhà phải tuân thủ những quy định về an toàn phòng chống cháy nổ, có trách nhiệm tham gia với lực lượng bảo vệ tòa nhà và lực lượng PCCC để ngăn chặn và xử lý kịp thời khi có sự cố xảy ra.

13. Mọi cư dẫn có trách nhiệm giữ gìn và bảo quản tài sản sở hữu chung, nếu làm hư hỏng phải có trách nhiệm bồi thường thiệt hại theo quy định.

14. Mọi cư dân phải có trách nhiệm thực hiện theo đúng nội quy chung cư, tòa nhà. Trường hợp cố tình vi phạm Ban quản lý tòa nhà sẽ từ chối cung cấp dịch vụ và xử lý theo Pháp luật.''';
}