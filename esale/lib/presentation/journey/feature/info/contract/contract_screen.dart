import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/journey/feature/info/document/widgets/document_item.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ContractScreen extends StatefulWidget {
  @override
  _ContractScreenState createState() => _ContractScreenState();
}

class _ContractScreenState extends State<ContractScreen> {
  void _onItemTap() {
    Routes.instance.navigateTo(RouteName.documentDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(StringConst.contract),
        iconRight: IconConst.question,
      ),
      body: ListView(
        children: [
          DocumentItem(
            icon: IconConst.word,
            text: 'Hợp đồng cho thuê',
            onTap: _onItemTap,
          ),
          const SizedBox(height: 16),
          DocumentItem(
            icon: IconConst.word,
            text: 'Hợp đồng bán nhà',
            onTap: _onItemTap,
          ),
          const SizedBox(height: 16),
          DocumentItem(
              icon: IconConst.word, text: 'Hợp đồng 1', onTap: _onItemTap),
        ],
      ),
    );
  }
}
