import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/installment_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/stype_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class InstallmentTenor extends StatefulWidget {
  @override
  _InstallmentTenorState createState() => _InstallmentTenorState();
}

class _InstallmentTenorState extends State<InstallmentTenor> {
  double _value = 5;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 20),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: PixelConstant.marginHorizontal),
          child: Text(
            translate(InstallmentConst.tenor),
            style: AppTextTheme.w500_16px,
          ),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _text(5),
            _text(10),
            _text(15),
            _text(20),
            _text(25),
            _text(30),
          ],
        ),
        Row(
          children: [
            Container(
              width: ScreenUtil.screenWidthDp,
              child: StyleSlider(
                child: Slider(
                  value: _value,
                  min: 5,
                  max: 30,
                  onChanged: (value) {
                    setState(() {
                      _value = value;
                    });
                  },
                  divisions: 5,

                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _text(var value) => Text(
        '$value',
        style: AppTextTheme.w500_14px.copyWith(
            color: value == _value ? AppColors.black : AppColors.grey7),
      );
}
