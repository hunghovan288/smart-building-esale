import 'dart:math';

import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/format_utils.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/installment_const.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/stype_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class InstallmentInterest extends StatefulWidget {
  @override
  _InstallmentInterestState createState() => _InstallmentInterestState();
}

class _InstallmentInterestState extends State<InstallmentInterest> {
  double valueDefault = 20;

  double _value = 8;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: PixelConstant.marginHorizontal),
          child: Row(
            children: [
              Text(
                translate(InstallmentConst.interest),
                style: AppTextTheme.w500_16px,
              ),
              const Spacer(),
              Text(
                '${_value.toInt()} %',
                style: AppTextTheme.w500_16px,
              ),
            ],
          ),
        ),
        StyleSlider(
          child: Slider(
            value: _value,
            onChanged: (value) {
              setState(() {
                _value = value;
              });
            },
            min: 0,
            max: valueDefault,
          ),
        )
      ],
    );
  }
}
