import 'dart:math';

import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/format_utils.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/installment_const.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_ranger.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/stype_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class InstallmentLayoutSlider extends StatefulWidget {
  final String label;
  final double value;
  final Widget widgetRight;

  const InstallmentLayoutSlider(
      {Key key, this.label, this.value, this.widgetRight})
      : super(key: key);

  @override
  _InstallmentLayoutSliderState createState() =>
      _InstallmentLayoutSliderState();
}

class _InstallmentLayoutSliderState extends State<InstallmentLayoutSlider> {
  double valueDefault;

  double _value;

  @override
  void initState() {
    valueDefault = widget.value;
    _value = valueDefault / 2;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: PixelConstant.marginHorizontal),
          child: Row(
            children: [
              Text(
                translate(widget.label),
                style: AppTextTheme.w500_16px,
              ),
              const Spacer(),
              Text(
                FormatUtils.formatCurrencyDoubleToString(
                    _value.toInt().toDouble() * pow(10, 6)),
                style: AppTextTheme.w500_16px,
              ),
              const SizedBox(width: 12),
              widget.widgetRight??SizedBox(),

            ],
          ),
        ),
        StyleSlider(
          child: Slider(
            value: _value,
            onChanged: (value) {
              setState(() {
                _value = value;
              });
            },
            min: 0,
            max: valueDefault,
          ),
        )
      ],
    );
  }
}
