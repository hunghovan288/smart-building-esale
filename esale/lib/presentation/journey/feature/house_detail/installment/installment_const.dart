class InstallmentConst {
  static const String _path = 'InstallmentConst.';
  static const String calInstallment = '${_path}calInstallment';
  static const String provisionalPayment = '${_path}provisionalPayment';
  static const String propertyPrice = '${_path}propertyPrice';
  static const String prepay = '${_path}prepay';
  static const String interest = '${_path}interest';
  static const String tenor = '${_path}tenor';
  static const String calculate = '${_path}calculate';
}
