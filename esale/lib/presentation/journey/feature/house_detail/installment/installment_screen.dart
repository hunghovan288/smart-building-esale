import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/installment_const.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/widgets/installment_slider.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/widgets/installment_interest.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/widgets/installment_tenor.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_common_button.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class InstallmentScreen extends StatefulWidget {
  @override
  _InstallmentScreenState createState() => _InstallmentScreenState();
}

class _InstallmentScreenState extends State<InstallmentScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(InstallmentConst.calInstallment),
        iconRight: IconConst.question,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 30),
            Column(
              children: [
                Text(
                  translate(InstallmentConst.provisionalPayment),
                  style:
                      AppTextTheme.w400_12px.copyWith(color: AppColors.grey7),
                ),
                const SizedBox(height: 14),
                GradientText(
                  text: '54,333,333',
                  style: AppTextTheme.w700_12px.copyWith(fontSize: 32),
                ),
                const SizedBox(height: 60),
                InstallmentLayoutSlider(
                  label: InstallmentConst.propertyPrice,
                  value: 1000000,
                  widgetRight: SvgPicture.asset(
                    IconConst.note,
                    width: 18,
                    height: 18,
                  ),
                ),
                const SizedBox(height: 30),
                InstallmentLayoutSlider(
                  label: InstallmentConst.propertyPrice,
                  value: 1000000,
                  widgetRight: Row(
                    children: [
                      Container(
                        width: 1,
                        height: 20,
                        color: AppColors.grey5,
                      ),
                      const SizedBox(width: 12),
                      Text(
                        '20%',
                        style: AppTextTheme.w500_16px
                            .copyWith(color: AppColors.grey5),
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 30),
                InstallmentInterest(),
                InstallmentTenor(),
                const SizedBox(height: 30),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PixelConstant.marginHorizontal),
                  child: CustomCommonButton(
                    text: translate(InstallmentConst.calculate),
                  ),
                ),
                SizedBox(
                  height: ScreenUtil.bottomBarHeight + 20,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
