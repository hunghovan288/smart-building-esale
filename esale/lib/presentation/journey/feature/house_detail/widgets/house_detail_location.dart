import 'package:esale/common/const/image_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HouseDetailLocation extends StatelessWidget {
  final HouseModel houseModel;

  const HouseDetailLocation({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          translate(StringConst.location),
          style: AppTextTheme.w700_16px,
        ),
        const SizedBox(height: 12),
        Text(
          houseModel.address,
          style: AppTextTheme.w400_14px,
        ),
        const SizedBox(height: 20),
        ClipRRect(
          borderRadius: BorderRadius.circular(12),
          child: Image.asset(
            MockConst.mockMap,
            width: double.infinity,
            height: 115,
            fit: BoxFit.cover,
          ),
        )
      ],
    );
  }
}
