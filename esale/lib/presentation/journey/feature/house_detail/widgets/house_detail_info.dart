import 'package:esale/common/const/string_const.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HouseDetailInfo extends StatelessWidget {
  final HouseModel houseModel;

  const HouseDetailInfo({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30),
        Text(
          translate(HouseDetailConst.detail),
          style: AppTextTheme.w700_16px,
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            _child(HouseDetailConst.status, houseModel.status),
            _child(HouseDetailConst.Electrical, houseModel.electrical),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            _child(HouseDetailConst.type, houseModel.type),
            _child(StringConst.cost, houseModel.cost),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            _child(HouseDetailConst.yearBuilt, houseModel.yearBuilt),
            _child(HouseDetailConst.area, houseModel.area),
          ],
        ),
        const SizedBox(height: 20),
        Row(
          children: [
            _child(HouseDetailConst.furniture, houseModel.furniture),
            _child(HouseDetailConst.floor, houseModel.floor),
          ],
        ),
        const SizedBox(height: 20),
      ],
    );
  }

  Widget _child(String label, String text) => Expanded(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              translate(label),
              style: AppTextTheme.w400_14px.copyWith(color: AppColors.grey7),
            ),
            const SizedBox(height: 8),
            Text(
              text ?? '',
              style: AppTextTheme.w400_14px,
            ),
          ],
        ),
      );
}
