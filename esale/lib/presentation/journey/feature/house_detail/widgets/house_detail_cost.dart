import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HouseDetailCost extends StatelessWidget {
  final HouseModel houseModel;
  final Function onCalInstallment;

  const HouseDetailCost({Key key, this.houseModel, this.onCalInstallment})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  translate(StringConst.cost),
                  style: AppTextTheme.w400_14px,
                ),
                const SizedBox(height: 6),
                GradientText(
                  text: houseModel.cost,
                  style: AppTextTheme.w700_20px,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: onCalInstallment,
            child: Padding(
              padding: const EdgeInsets.only(top: 12, bottom: 12, left: 12),
              child: Column(
                children: [
                  Row(
                    children: [
                      SvgPicture.asset(
                        IconConst.houseCalInstallment,
                        width: 24,
                        height: 24,
                      ),
                      const SizedBox(width: 6),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            houseModel.installment,
                            style: AppTextTheme.w400_14px,
                          ),
                          Text(
                            translate(HouseDetailConst.calInstallment),
                            style: AppTextTheme.w400_12px.copyWith(
                                fontSize: 10, color: AppColors.primaryLight),
                          )
                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
