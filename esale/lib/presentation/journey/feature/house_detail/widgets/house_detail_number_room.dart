import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HomeDetailNumberRoom extends StatelessWidget {
  final HouseModel houseModel;

  const HomeDetailNumberRoom({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(height: 1, color: AppColors.grey4),
        Row(
          children: [
            Expanded(
                child: _item(
                    '${houseModel.numberRoom} ${translate(StringConst.bedRoom)}',
                    IconConst.searchDetailBedRoom)),
            Container(
              width: 1,
              height: 40,
              color: AppColors.grey4,
            ),
            Expanded(
                child: _item(
                    '${houseModel.numberToilet} ${translate(HouseDetailConst.toilet)}',
                    IconConst.searchDetailBathRoom)),
            Container(
              width: 1,
              height: 40,
              color: AppColors.grey4,
            ),
            Expanded(
                child: _item(
                    '${houseModel.installment}', IconConst.searchDetailArea)),
          ],
        ),
        Divider(height: 1, color: AppColors.grey4),
      ],
    );
  }

  Widget _item(String text, String icon) => Column(
        children: [
          const SizedBox(height: 12),
          SvgPicture.asset(
            icon,
            width: 24,
            height: 24,
            color: AppColors.black,
          ),
          const SizedBox(height: 8),
          Text(
            text ?? '',
            style: AppTextTheme.w400_12px,
          ),
          const SizedBox(height: 12),
        ],
      );
}
