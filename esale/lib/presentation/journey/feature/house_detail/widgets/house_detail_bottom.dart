import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HouseDetailBottom extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70 + ScreenUtil.bottomBarHeight,
      decoration: BoxDecoration(color: AppColors.white, boxShadow: [
        BoxShadow(
          color: AppColors.black.withOpacity(0.05),
          spreadRadius: 3,
          blurRadius: 3,
          offset: Offset(0, -2),
        )
      ]),
      padding: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      child: Row(
        children: [
          CustomImageNetwork(
            url: MockConst.urlPersons[1],
            width: 44,
            height: 44,
            fit: BoxFit.cover,
          ),
          const SizedBox(width: 12),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Dieu Linh',
                style: AppTextTheme.w500_12px,
              ),
              const SizedBox(height: 4),
              Text(
                translate(HouseDetailConst.interested),
                style: AppTextTheme.w400_12px,
              )
            ],
          ),
          const Spacer(),
          Container(
            width: 115,
            height: 36,
            decoration: BoxDecoration(
                gradient: CommonUtil.getGradient(),
                borderRadius: BorderRadius.circular(8)),
            child: Center(
              child: Text(
                translate(HouseDetailConst.makeAContract),
                style: AppTextTheme.w400_12px.copyWith(color: AppColors.white),
              ),
            ),
          )
        ],
      ),
    );
  }
}
