import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_const.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class HouseDetailFeature extends StatelessWidget {
  final HouseModel houseModel;

  const HouseDetailFeature({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 30),
        Text(
          translate(StringConst.description),
          style: AppTextTheme.w700_16px,
        ),
        const SizedBox(height: 12),
        Text(
          houseModel.description,
          style: AppTextTheme.w400_14px,
        ),
        const SizedBox(height: 20),
        Text(
          translate(StringConst.feature),
          style: AppTextTheme.w700_16px,
        ),
        const SizedBox(height: 16),
        Row(
          children: [
            _itemFeature(HouseDetailConst.park, IconConst.housePark),
            const SizedBox(width: 12),
            _itemFeature(HouseDetailConst.security, IconConst.houseSecurity),
            const SizedBox(width: 12),
            _itemFeature(HouseDetailConst.balcony, IconConst.houseBalcony),
          ],
        ),
        const SizedBox(height: 12),
        Row(
          children: [
            _itemFeature(HouseDetailConst.swimmingPool, IconConst.houseSwimmingPool),
            const SizedBox(width: 12),
            _itemFeature(HouseDetailConst.furniture, IconConst.houseFurniture),
            const SizedBox(width: 12),
            _itemFeature(HouseDetailConst.CCTV, IconConst.houseCctv),
          ],
        ),
      ],
    );
  }

  Widget _itemFeature(String text, String icon) => Expanded(
        child: Container(
          height: 40,
          padding: EdgeInsets.symmetric(horizontal: 12),
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.grey4, width: 1),
              borderRadius: BorderRadius.circular(4)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon,
                width: 24,
                height: 24,
                fit: BoxFit.cover,
              ),
              const SizedBox(width: 4),
              Text(
                translate(text),
                style: AppTextTheme.w400_12px,
              ),
            ],
          ),
        ),
      );
}
