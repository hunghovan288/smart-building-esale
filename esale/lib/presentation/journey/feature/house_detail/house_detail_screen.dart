import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_bottom.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_cost.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_feature.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_info.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_location.dart';
import 'package:esale/presentation/journey/feature/house_detail/widgets/house_detail_number_room.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HouseDetailScreen extends StatefulWidget {
  @override
  _HouseDetailScreenState createState() => _HouseDetailScreenState();
}

class _HouseDetailScreenState extends State<HouseDetailScreen> {
  HouseModel _houseModel;

  @override
  void initState() {
    _houseModel = MockConst.mockHouses()[0];
    super.initState();
  }

  void _onCalInstallment() {
    Routes.instance.navigateTo(RouteName.installmentScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: '',
        iconRight: IconConst.share,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PixelConstant.marginHorizontal, vertical: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GradientText(
                        text:
                            'Căn hộ ${_houseModel.numberRoom}PN - ${_houseModel.name}',
                        style: AppTextTheme.w700_12px.copyWith(fontSize: 20),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PixelConstant.marginHorizontal),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        IconConst.searchLocation,
                        width: 14,
                        height: 14,
                      ),
                      const SizedBox(width: 6),
                      Text(
                        _houseModel.address,
                        style: AppTextTheme.w400_14px,
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(vertical: 16),
                  height: 200,
                  child: ListView.separated(
                    itemBuilder: (_, index) => CustomImageNetwork(
                      url: _houseModel.listImage[index],
                      width: ScreenUtil.screenWidthDp * 0.8,
                      height: 200,
                      fit: BoxFit.cover,
                    ),
                    separatorBuilder: (_, i) => const SizedBox(width: 12),
                    itemCount: _houseModel.listImage.length,
                    scrollDirection: Axis.horizontal,
                    padding: EdgeInsets.symmetric(
                        horizontal: PixelConstant.marginHorizontal),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: PixelConstant.marginHorizontal),
                  child: Column(
                    children: [
                      HouseDetailCost(
                        houseModel: _houseModel,
                        onCalInstallment: _onCalInstallment,
                      ),
                      HomeDetailNumberRoom(
                        houseModel: _houseModel,
                      ),
                      HouseDetailFeature(
                        houseModel: _houseModel,
                      ),
                      HouseDetailInfo(
                        houseModel: _houseModel,
                      ),
                      HouseDetailLocation(
                        houseModel: _houseModel,
                      ),
                      const SizedBox(height: 120),
                    ],
                  ),
                )
              ],
            ),
          ),
          Positioned(
            child: HouseDetailBottom(),
            bottom: 0,
            left: 0,
            right: 0,
          ),
        ],
      ),
    );
  }
}
