class HouseDetailConst {
  static const String _path = 'HouseDetailConst.';
  static const String calInstallment = '${_path}calInstallment';
  static const String toilet = '${_path}toilet';
  static const String park = '${_path}park';
  static const String security = '${_path}security';
  static const String balcony = '${_path}balcony';
  static const String swimmingPool = '${_path}swimmingPool';
  static const String furniture = '${_path}furniture';
  static const String CCTV = 'CCTV';
  static const String detail = '${_path}detail';
  static const String status = '${_path}status';
  static const String Electrical = 'Electrical';
  static const String type = '${_path}type';
  static const String yearBuilt = '${_path}yearBuilt';
  static const String area = '${_path}area';
  static const String floor = '${_path}floor';
  static const String interested = '${_path}interested';
  static const String makeAContract = '${_path}makeAContract';
}
