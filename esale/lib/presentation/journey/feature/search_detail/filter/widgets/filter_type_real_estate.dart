import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_outline_border_text.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class FilterRealEstateType extends StatefulWidget {
  @override
  _FilterRealEstateTypeState createState() => _FilterRealEstateTypeState();
}

class _FilterRealEstateTypeState extends State<FilterRealEstateType> {
  RealEstateType _type = RealEstateType.APARTMENT;

  void _onTap(type) {
    setState(() {
      _type = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translate(FilterConst.realEstateType),
            style: AppTextTheme.w500_16px,
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              _typeWidget(
                text: translate(FilterConst.house),
                type: RealEstateType.HOUSE,
                onTap: _onTap,
              ),
              const SizedBox(width: 16),
              _typeWidget(
                text: translate(FilterConst.apartment),
                type: RealEstateType.APARTMENT,
                onTap: _onTap,
              ),
              const SizedBox(width: 16),
              _typeWidget(
                text: translate(FilterConst.shopHouse),
                type: RealEstateType.SHOP_HOUSE,
                onTap: _onTap,
              ),
            ],
          ),
          const SizedBox(height: 16),
          Row(
            children: [
              _typeWidget(
                text: translate(FilterConst.pentHouse),
                type: RealEstateType.PENT_HOUSE,
                onTap: _onTap,
              ),
              const SizedBox(width: 16),
              _typeWidget(
                text: translate(FilterConst.villa),
                type: RealEstateType.VILLA,
                onTap: _onTap,
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _typeWidget(
          {String text,
          RealEstateType type,
          Function(RealEstateType type) onTap}) =>
      InkWell(
        onTap: () {
          onTap(type);
        },
        child: OutLineBorderText(
          strokeWidth: 1,
          radius: 6,
          text: text,
          gradient: CommonUtil.getGradient(
              colors:
                  _type != type ? [AppColors.grey4, AppColors.grey4] : null),
          child: GradientText(
            text: text ?? '',
            style:
                _type != type ? AppTextTheme.w400_12px : AppTextTheme.w500_12px,
            colors: _type != type
                ? [AppColors.black, AppColors.black]
                : AppColors.colorsGradient,
          ),
        ),
      );
}
