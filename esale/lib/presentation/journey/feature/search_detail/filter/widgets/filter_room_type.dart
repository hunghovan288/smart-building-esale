import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class FilterRoomType extends StatefulWidget {
  final String label;

  const FilterRoomType({Key key, this.label}) : super(key: key);

  @override
  _FilterRoomTypeState createState() => _FilterRoomTypeState();
}

class _FilterRoomTypeState extends State<FilterRoomType> {
  int _numberRoom = 0;

  void _onItemTap(int number) {
    setState(() {
      _numberRoom = number;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal, vertical: 12),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            translate(widget.label),
            style: AppTextTheme.w500_16px,
          ),
          const SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
                color: AppColors.grey3, borderRadius: BorderRadius.circular(8)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _typeWidget(
                    text: translate(StringConst.all),
                    onTap: _onItemTap,
                    numberRoom: 0),
                _typeWidget(text: '1', onTap: _onItemTap, numberRoom: 1),
                _typeWidget(text: '2', onTap: _onItemTap, numberRoom: 2),
                _typeWidget(text: '3', onTap: _onItemTap, numberRoom: 3),
                _typeWidget(text: '+4', onTap: _onItemTap, numberRoom: 4),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _typeWidget({
    String text,
    int numberRoom,
    Function(int number) onTap,
  }) =>
      Expanded(
        child: InkWell(
          onTap: () {
            onTap(numberRoom);
          },
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(numberRoom == 0 ? 8 : 0),
                    bottomLeft: Radius.circular(numberRoom == 0 ? 8 : 0),
                    topRight: Radius.circular(
                        (numberRoom == _numberRoom || numberRoom == 4) ? 8 : 0),
                    bottomRight: Radius.circular(
                      (numberRoom == _numberRoom || numberRoom == 4) ? 8 : 0,
                    )),
                gradient: numberRoom <= _numberRoom
                    ? CommonUtil.getGradient()
                    : CommonUtil.getGradient(colors: [
                        AppColors.grey3,
                        AppColors.grey3,
                      ])),
            height: 44,
            child: Center(
              child: Text(
                text,
                style: AppTextTheme.w400_12px.copyWith(
                  fontSize: 16,
                  color: numberRoom <= _numberRoom
                      ? AppColors.white
                      : AppColors.black,
                ),
              ),
            ),
          ),
        ),
      );
}
