import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

const maxValue = 200.0;

class FilterRanger extends StatefulWidget {
  @override
  _FilterRangerState createState() => _FilterRangerState();
}

class _FilterRangerState extends State<FilterRanger> {
  var rangeValue = RangeValues(0.2, 0.8);

  @override
  Widget build(BuildContext context) {
    String billion = translate(StringConst.billion);
    return Column(
      children: [
        const SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Row(
            children: [
              Text(
                translate(StringConst.cost),
                style: AppTextTheme.w500_16px,
              ),
              const Spacer(),
              Text(
                '${_formatSpace(rangeValue.start * maxValue)} $billion  -  ${_formatSpace(rangeValue.end * maxValue)} $billion',
                style: AppTextTheme.w500_14px,
              )
            ],
          ),
        ),
        SliderTheme(
          data: const SliderThemeData(
            trackHeight: 3.0,
            inactiveTrackColor: AppColors.grey4,
            rangeTrackShape: RectangularRangeSliderTrackShape(),
            rangeThumbShape: RoundRangeSliderThumbShape(
              enabledThumbRadius: 12,
            ),
          ),
          child: RangeSlider(
            values: rangeValue,
            onChanged: (RangeValues value) {
              setState(() {
                rangeValue = value;
              });
            },
          ),
        ),
      ],
    );
  }

  String _formatSpace(double input) {
    if (input > 10.0) {
      return '${input.toInt()}';
    }
    return '$input  '.substring(0, 3);
  }
}
