import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/stype_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class FilterSelectArea extends StatefulWidget {
  @override
  _FilterSelectAreaState createState() => _FilterSelectAreaState();
}

class _FilterSelectAreaState extends State<FilterSelectArea> {
  double _value = 200;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 12),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: PixelConstant.marginHorizontal),
          child: Text(
            '${translate(FilterConst.acreage)} (M2)',
            style: AppTextTheme.w500_16px,
          ),
        ),
        const SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            _text(0),
            _text(100),
            _text(200),
            _text(300),
            _text(400),
          ],
        ),
        Row(
          children: [
            Container(
              width: ScreenUtil.screenWidthDp,
              child: StyleSlider(
                child: Slider(
                  value: _value,
                  min: 0,
                  max: 400,
                  onChanged: (value) {
                    setState(() {
                      _value = value;
                      LOG.d('$_value');
                    });
                  },
                  divisions: 4,
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: ScreenUtil.bottomBarHeight + 100,
        )
      ],
    );
  }

  Widget _text(var value) => Text(
        '$value',
        style: AppTextTheme.w500_14px.copyWith(
            color: value == _value ? AppColors.black : AppColors.grey7),
      );
}
