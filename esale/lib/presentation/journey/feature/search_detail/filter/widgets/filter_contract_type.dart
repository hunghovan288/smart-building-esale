import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class FilterContractType extends StatefulWidget {
  @override
  _FilterContractTypeState createState() => _FilterContractTypeState();
}

class _FilterContractTypeState extends State<FilterContractType> {
  FilterTypeContact _contractType = FilterTypeContact.SELL;

  void _onTypeTap(type) {
    setState(() {
      _contractType = type;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          horizontal: PixelConstant.marginHorizontal, vertical: 20),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                translate(FilterConst.contract),
                style: AppTextTheme.w500_16px,
              ),
              const Spacer(),
              Container(
                padding: EdgeInsets.all(2),
                decoration: BoxDecoration(
                    color: AppColors.grey3,
                    borderRadius: BorderRadius.circular(6)),
                child: Row(
                  children: [
                    _type(
                      text: translate(FilterConst.rent),
                      type: FilterTypeContact.RENT,
                      onTap: _onTypeTap,
                    ),
                    _type(
                      text: translate(FilterConst.sell),
                      type: FilterTypeContact.SELL,
                      onTap: _onTypeTap,
                    ),
                  ],
                ),
              )
            ],
          ),
          const SizedBox(height: 20),
          Divider(
            height: 3,
            color: AppColors.grey5,
          ),
        ],
      ),
    );
  }

  Widget _type(
          {String text,
          Function(FilterTypeContact type) onTap,
          FilterTypeContact type}) =>
      InkWell(
        onTap: () {
          onTap(type);
        },
        child: Container(
          width: 94,
          height: 40,
          decoration: BoxDecoration(
              color: _contractType == type ? AppColors.white : AppColors.grey3,
              borderRadius: BorderRadius.circular(6)),
          child: Center(
            child: Text(
              text,
              style: AppTextTheme.w500_12px.copyWith(
                  color: _contractType == type
                      ? AppColors.black
                      : AppColors.grey7),
            ),
          ),
        ),
      );
}
