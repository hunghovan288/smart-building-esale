class FilterConst{
  static const String _path ='FilterConst.';
  static const String filter ='${_path}filter';
  static const String cancelFilter ='${_path}cancelFilter';
  static const String contract ='${_path}contract';
  static const String rent ='${_path}rent';
  static const String sell ='${_path}sell';
  static const String realEstateType ='${_path}realEstateType';
  static const String house ='${_path}house';
  static const String apartment ='${_path}apartment';
  static const String shopHouse ='${_path}shopHouse';
  static const String pentHouse ='${_path}pentHouse';
  static const String villa ='${_path}villa';
  static const String acreage ='${_path}acreage';
  static const String filterRealEstate ='${_path}filterRealEstate';

}