import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_const.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_contract_type.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_ranger.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_room_type.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_select_area.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/widgets/filter_type_real_estate.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_common_button.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class FilterScreen extends StatefulWidget {
  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  void _onUnfilter() {
    Routes.instance.pop();
  }

  void _onFilter() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(FilterConst.filter),
        widgetRight: InkWell(
          onTap: _onUnfilter,
          child: Padding(
            padding:
                const EdgeInsets.only(right: 16, left: 16, top: 12, bottom: 12),
            child: Text(
              translate(FilterConst.cancelFilter),
              style: AppTextTheme.w600_16px
                  .copyWith(fontSize: 12, color: AppColors.pink),
            ),
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Divider(color: AppColors.grey5, height: 2),
                FilterContractType(),
                FilterRealEstateType(),
                FilterRanger(),
                FilterRoomType(
                  label: StringConst.bedRoom,
                ),
                const SizedBox(height: 12),
                FilterRoomType(
                  label: StringConst.bathRoom,
                ),
                FilterSelectArea(),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                      color: AppColors.black.withOpacity(0.05),
                      offset: Offset(0, -3),
                      blurRadius: 2,
                      spreadRadius: 3)
                ],
                color: AppColors.white,
              ),
              height: 80,
              child: Center(
                child: CustomCommonButton(
                  text: translate(FilterConst.filterRealEstate),
                  onTap: _onFilter,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
