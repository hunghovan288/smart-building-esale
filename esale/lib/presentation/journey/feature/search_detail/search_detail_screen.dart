import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/search_detail/search_detail_const.dart';
import 'package:esale/presentation/journey/feature/search_detail/widgets/search_detail_item.dart';
import 'package:esale/presentation/journey/feature/search_detail/widgets/search_detail_textfield.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchDetailScreen extends StatefulWidget {
  @override
  _SearchDetailScreenState createState() => _SearchDetailScreenState();
}

class _SearchDetailScreenState extends State<SearchDetailScreen> {
  void _onFilter() {
    Routes.instance.navigateTo(RouteName.filterScreen);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: true,
      body: Stack(
        children: [
          Column(
            children: [
              SizedBox(height: ScreenUtil.statusBarHeight + 20),
              SearchDetailTextField(),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: PixelConstant.marginHorizontal, vertical: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            translate(SearchDetailConst.resultSearch),
                            style: AppTextTheme.w600_16px,
                          ),
                          const SizedBox(height: 8),
                          Text(
                            '94 ${translate(SearchDetailConst.realEstate)}',
                            style:
                                AppTextTheme.w400_14px.copyWith(fontSize: 12),
                          )
                        ],
                      ),
                    ),
                    InkWell(
                      onTap: _onFilter,
                      child: Image.asset(
                        IconConst.searchDetailFilter,
                        width: 97,
                        height: 36,
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: ListView.separated(
                    padding: EdgeInsets.symmetric(
                        horizontal: PixelConstant.marginHorizontal,
                        vertical: 16),
                    itemBuilder: (_, index) => SearchDetailItem(
                          houseModel: MockConst.mockHouses()[index],
                        ),
                    separatorBuilder: (_, i) => const SizedBox(
                          height: 16,
                        ),
                    itemCount: MockConst.mockHouses().length),
              )
            ],
          ),
          Positioned(
              bottom: ScreenUtil.statusBarHeight ,
              right: 0,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Container(
                  width: 40,
                  height: 40,
                  decoration: BoxDecoration(
                    gradient: CommonUtil.getGradient(),
                    shape: BoxShape.circle
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      IconConst.searchDetailCurrentLocation,
                      width: 24,
                      height: 24,
                    ),
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
