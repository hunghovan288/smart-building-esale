import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/journey/feature/search_detail/search_detail_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchDetailTextField extends StatelessWidget {
  final Function onBack;

  const SearchDetailTextField({Key key, this.onBack}) : super(key: key);

  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      width: double.infinity,
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: CommonConst.defaultShadow,
          borderRadius: BorderRadius.circular(8)),
      child: Row(
        children: [
          InkWell(
            onTap: _onBack,
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: SvgPicture.asset(
                IconConst.arrowBack,
                width: 16,
                height: 16,
              ),
            ),
          ),
          Expanded(
              child: TextFormField(
                  style: AppTextTheme.w400_14px,
                  cursorColor: AppColors.primaryColor,
                  decoration: InputDecoration(
                    hintText: translate(SearchDetailConst.search),
                    contentPadding: EdgeInsets.only(bottom: 4,left: 4),
                    filled: true,
                    fillColor: Colors.white,
                    hintStyle:
                        AppTextTheme.w400_14px.copyWith(color: AppColors.grey7),
                    border: InputBorder.none,
                  ))),
          const SizedBox(width: 8),
        ],
      ),
    );
  }
}
