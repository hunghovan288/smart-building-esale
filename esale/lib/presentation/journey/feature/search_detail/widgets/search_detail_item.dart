import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class SearchDetailItem extends StatelessWidget {
  final HouseModel houseModel;

  const SearchDetailItem({Key key, this.houseModel}) : super(key: key);
  void _onItemTap(){
    Routes.instance.navigateTo(RouteName.houseDetailScreen);
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onItemTap,
      child: Container(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                CustomImageNetwork(
                  url: houseModel.background,
                  width: double.infinity,
                  height: 200,
                  fit: BoxFit.cover,
                ),
                Positioned(
                  bottom: 12,right: 12,
                    child: Container(
                  width: 93,
                  height: 30,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      gradient: CommonUtil.getGradient(
                          colors: AppColors.colorsGradient)),
                      child: Center(
                        child:Text(
                          '2 ~ 3 Tỷ',
                          style: AppTextTheme.w500_14px.copyWith(color: AppColors.white),
                        ),
                      ),
                ))
              ],
            ),
            const SizedBox(height: 16),
            GradientText(
              text:
                  '${houseModel.numberRoom}${translate(SearchConst.bedroom)} - ${houseModel.name}',
              colors: AppColors.colorsGradient,
              style: AppTextTheme.w600_16px,
              gradientDirection: GradientDirection.ttb,
            ),
            const SizedBox(height: 8),
            Row(
              children: [
                SvgPicture.asset(
                  IconConst.searchLocation,
                  width: 14,
                  height: 14,
                  color: AppColors.grey7,
                ),
                const SizedBox(width: 4),
                Text(
                  houseModel.address ?? '',
                  style: AppTextTheme.w400_14px.copyWith(color: AppColors.grey7),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 12),
              child: Divider(
                color: AppColors.grey4,
                height: 1,
              ),
            ),
            Row(
              children: [
                SvgPicture.asset(
                  IconConst.searchDetailBedRoom,
                  width: 20,
                  height: 20,
                ),
                Text(
                  '3',
                  style: AppTextTheme.w500_14px
                      .copyWith(color: AppColors.grey7, fontSize: 12),
                ),
                const SizedBox(width: 16),
                SvgPicture.asset(
                  IconConst.searchDetailBathRoom,
                  width: 20,
                  height: 20,
                ),
                Text(
                  '2',
                  style: AppTextTheme.w500_14px
                      .copyWith(color: AppColors.grey7, fontSize: 12),
                ),
                const SizedBox(width: 16),
                SvgPicture.asset(
                  IconConst.searchDetailArea,
                  width: 20,
                  height: 20,
                ),
                Text(
                  '170 M',
                  style: AppTextTheme.w500_14px
                      .copyWith(color: AppColors.grey7, fontSize: 12),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
