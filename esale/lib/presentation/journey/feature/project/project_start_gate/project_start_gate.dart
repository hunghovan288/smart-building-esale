import 'package:flutter/material.dart';
import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/presentation/journey/feature/project/widgets/project_item.dart';

class ProjectStartGateTab extends StatefulWidget {
  @override
  _ProjectStartGateTabState createState() => _ProjectStartGateTabState();
}

class _ProjectStartGateTabState extends State<ProjectStartGateTab> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: ListView(
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 0),
        children: [
          ProjectItem(
            houseModel: MockConst.mockHouses()[0],
            projectStatus: ProjectStatus.ON_SALE,
          ),
          Divider(),
          ProjectItem(
            houseModel: MockConst.mockHouses()[1],
            projectStatus: ProjectStatus.DESIGNED,
          ),
          Divider(),
          ProjectItem(
            houseModel: MockConst.mockHouses()[1],
            projectStatus: ProjectStatus.START_GATE,
          ),
        ],
      ),
    );
  }
}
