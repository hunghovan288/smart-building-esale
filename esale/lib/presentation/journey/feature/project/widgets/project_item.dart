import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/project/widgets/project_widget_status.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProjectItem extends StatelessWidget {
  final HouseModel houseModel;
  final ProjectStatus projectStatus;

  const ProjectItem({Key key, this.houseModel, this.projectStatus})
      : super(key: key);
  void _onItemTab(){
    Routes.instance.navigateTo(RouteName.projectDetailScreen);
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onItemTab,
      child: Container(
        width: double.infinity,
        height: 145,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: PixelConstant.marginHorizontal),
              child: CustomImageNetwork(
                url: houseModel.background,
                height: 105,
                width: 135,
                fit: BoxFit.cover,
                borderRadius: BorderRadius.circular(8),
              ),
            ),
            Expanded(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    ProjectWidgetStatus(
                      status: projectStatus,
                    ),
                    const SizedBox(width: 20),
                    Text(
                      '21/05/2021',
                      style:
                          AppTextTheme.w500_12px.copyWith(color: AppColors.grey),
                    )
                  ],
                ),
                const SizedBox(height: 8),
                Text(
                  houseModel.name,
                  style: AppTextTheme.w700_14px,
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    SvgPicture.asset(
                      IconConst.searchDetailArea,
                      width: 14,
                      height: 14,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      '54.000 M2',
                      style: AppTextTheme.w400_12px,
                    )
                  ],
                ),
                const SizedBox(height: 8),
                Row(
                  children: [
                    SvgPicture.asset(
                      IconConst.searchLocation,
                      width: 14,
                      height: 14,
                    ),
                    const SizedBox(width: 4),
                    Text(
                      houseModel.address ?? '',
                      style: AppTextTheme.w400_12px,
                    )
                  ],
                )
              ],
            ))
          ],
        ),
      ),
    );
  }
}
