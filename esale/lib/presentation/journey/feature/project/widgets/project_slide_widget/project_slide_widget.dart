import 'package:carousel_slider/carousel_slider.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/presentation/journey/feature/project/widgets/project_slide_widget/project_slide_item.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';

class ProjectSlideWidget extends StatefulWidget {
  final List<String> images;
  final double borderRadius;
  final double height;
  final Widget displayNumberImage;
  final bool autoPlay;
  final bool revert;
  final Duration duration;
  final bool enableInfiniteScroll;
  final Function(int index) onchangePage;
  final BoxFit fit;

  const ProjectSlideWidget(
      {Key key,
      this.images,
      this.borderRadius = 0,
      this.height,
      this.displayNumberImage,
      this.autoPlay = false,
      this.revert = false,
      this.duration,
      this.enableInfiniteScroll = false,
      this.onchangePage,
      this.fit})
      : super(key: key);

  @override
  _ProjectSlideWidgetState createState() => _ProjectSlideWidgetState();
}

class _ProjectSlideWidgetState extends State<ProjectSlideWidget> {
  int _currentSlideIndex = 0;

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.images == null || widget.images.isEmpty) {
      return const SizedBox();
    }
    return Column(
      children: [
        Container(
          width: double.infinity,
          margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
          height: 170,
          child: CarouselSlider(
            options: CarouselOptions(
              initialPage: 0,
              height: double.infinity,
              autoPlay: widget.autoPlay,
              autoPlayAnimationDuration: widget.duration,
              viewportFraction: 1.0,
              reverse: widget.revert,
              enableInfiniteScroll: widget.enableInfiniteScroll,
              onPageChanged: (index, reason) {
                if (widget.onchangePage != null) {
                  widget.onchangePage(index);
                }
                setState(() {
                  _currentSlideIndex = index;
                });
              },
            ),
            items: widget.images
                .map((String value) => ProjectSlideItem(
                      url: value,
                    ))
                .toList(),
          ),
        ),
        const SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children:
              widget.images.map((e) => _dot(widget.images.indexOf(e))).toList(),
        )
      ],
    );
  }

  Widget _dot(int index) => Container(
        width: index == _currentSlideIndex ? 8 : 6,
        margin: EdgeInsets.symmetric(horizontal: 6),
        height: index == _currentSlideIndex ? 8 : 6,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color:
                index == _currentSlideIndex ? AppColors.black : AppColors.grey),
      );
}
