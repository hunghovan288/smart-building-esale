import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';

class ProjectSlideItem extends StatelessWidget {
  final String url;

  const ProjectSlideItem({Key key, this.url}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Container(
        width: double.infinity,
        height: 170,
        child: Stack(
          children: [
            CustomImageNetwork(
              url: url,
              width: double.infinity,
              height: 170,
              fit: BoxFit.cover,
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                width: double.infinity,
                height: 50,
                decoration: BoxDecoration(boxShadow: [
                  BoxShadow(
                      color: AppColors.black.withOpacity(0.3),
                      blurRadius: 5,
                      spreadRadius: 5,
                      )
                ]),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 12, vertical: 5),
                      child: Text(
                        'Đang mở bán',
                        style: AppTextTheme.w400_12px
                            .copyWith(fontSize: 10, color: AppColors.white),
                      ),
                    ),
                    decoration: BoxDecoration(
                        color: AppColors.green,
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  const Spacer(),
                  Text(
                    'St Moritz - Khu căn hộ và văn phòng hạng sang',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style:
                        AppTextTheme.w500_16px.copyWith(color: AppColors.white),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
