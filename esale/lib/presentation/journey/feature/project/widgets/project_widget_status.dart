import 'package:esale/common/const/enum.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/journey/feature/project/project_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ProjectWidgetStatus extends StatelessWidget {
  final ProjectStatus status;

  const ProjectWidgetStatus({Key key, this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 88,
      height: 20,
      decoration: BoxDecoration(
          color: _mapColorByStatus(), borderRadius: BorderRadius.circular(4)),
      child: Center(
        child: Text(
          translate(ProjectUtils.getStringByStatus(status)),
          style: AppTextTheme.w400_12px
              .copyWith(fontSize: 10, color: AppColors.white),
        ),
      ),
    );
  }

  Color _mapColorByStatus() {
    if (status == ProjectStatus.ON_SALE) {
      return AppColors.green;
    } else if (status == ProjectStatus.DESIGNED) {
      return AppColors.yellow;
    } else {
      return AppColors.primaryColor;
    }
  }
}
