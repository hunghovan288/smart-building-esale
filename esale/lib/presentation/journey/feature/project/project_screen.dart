import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/presentation/journey/feature/project/project_all/project_all_tab.dart';
import 'package:esale/presentation/journey/feature/project/project_are_designed/project_are_designed_tab.dart';
import 'package:esale/presentation/journey/feature/project/project_on_sale/project_on_sale.dart';
import 'package:esale/presentation/journey/feature/project/project_start_gate/project_start_gate.dart';
import 'package:esale/presentation/journey/feature/project/widgets/project_slide_widget/project_slide_item.dart';
import 'package:esale/presentation/journey/feature/project/widgets/project_slide_widget/project_slide_widget.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/custom_tabbar_view.dart';
import 'package:esale/presentation/widgets/keep_alive_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ProjectScreen extends StatefulWidget {
  @override
  _ProjectScreenState createState() => _ProjectScreenState();
}

class _ProjectScreenState extends State<ProjectScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      customAppBar: CustomAppBar(
        onBack: () {
          Routes.instance.pop();
        },
        title: translate(SearchConst.project).toUpperCase(),
        colorTitle: AppColors.primaryColor,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 12),
            ProjectSlideWidget(
              images: MockConst.imageHouse,
            ),
            const SizedBox(height: 16),
            TabViewBottom(
              height: 480,
              stretchCenter: false,
              tabs: [
                KeepAliveWidget(child: ProjectAllTab()),
                KeepAliveWidget(child: ProjectOnSale()),
                KeepAliveWidget(child: ProjectAreDesigned()),
                KeepAliveWidget(child: ProjectStartGateTab()),
              ],
              titlesTab: [
                StringConst.all,
                StringConst.onSale,
                StringConst.onDesign,
                StringConst.startGate,
              ],
            )
          ],
        ),
      ),
    );
  }
}
