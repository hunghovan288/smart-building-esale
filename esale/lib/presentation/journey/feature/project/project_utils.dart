import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/string_const.dart';

class ProjectUtils {
  static String getStringByStatus(ProjectStatus status) {
    if (status == ProjectStatus.ON_SALE) {
      return StringConst.onSale;
    } else if (status == ProjectStatus.DESIGNED) {
      return StringConst.onDesign;
    } else {
      return StringConst.startGate;
    }
  }
}
