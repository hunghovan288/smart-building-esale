import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NewsAppBar extends StatelessWidget {
  void _onSearch(){

  }
  void _onMore(){

  }
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
      height: defaultAppbar + ScreenUtil.statusBarHeight,
      child: Row(
        children: [
          InkWell(
            onTap: (){
              Routes.instance.pop();
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 12),
              child: SvgPicture.asset(
                IconConst.arrowBack,
                width: 20,
                height: 20,
              ),
            ),
          ),
          const Spacer(),
          InkWell(
            onTap: _onSearch,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 16, right: 8, bottom: 12, top: 12),
              child: SvgPicture.asset(
                IconConst.share,
                width: 24,
                height: 24,
                color: AppColors.black,
              ),
            ),
          ),
          InkWell(
            onTap: _onMore,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 8, right: 16, bottom: 12, top: 12),
              child: Icon(
                Icons.more_vert,
                size: 24,
                color: AppColors.black,
              ),
            ),
          )
        ],
      ),
    );
  }
}
