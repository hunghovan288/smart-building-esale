import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/news/widgets/news_appbar.dart';
import 'package:esale/presentation/journey/feature/news/widgets/news_tag.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NewsScreen extends StatefulWidget {
  @override
  _NewsScreenState createState() => _NewsScreenState();
}

class _NewsScreenState extends State<NewsScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Column(
        children: [
          NewsAppBar(),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PixelConstant.marginHorizontal),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Mở bán dự án chung cư  Opal Boulevard',
                          style: AppTextTheme.w700_20px,
                        ),
                        const SizedBox(height: 16),
                        Row(
                          children: [
                            Container(
                              width: 88,
                              height: 24,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(2),
                                gradient: CommonUtil.getGradient(),
                              ),
                              child: Center(
                                child: Text(
                                  'Tin tức BĐS',
                                  style: AppTextTheme.w400_10px
                                      .copyWith(color: AppColors.white),
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Icon(
                              Icons.calendar_today,
                              size: 16,
                              color: AppColors.grey,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              '10/26/20',
                              style: AppTextTheme.w500_12px.copyWith(
                                  fontSize: 10, color: AppColors.grey),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 24),
                          child: CustomImageNetwork(
                            url: MockConst.imageHouse[2],
                            width: ScreenUtil.screenWidthDp - 32,
                            height: 170,
                            fit: BoxFit.cover,
                            border: 5,
                          ),
                        ),
                        Text(
                          '''Tọa lạc trên cung đường vàng Phạm Văn Đồng, Opal Boulevard sở hữu vị trí giao thương chiến lược cùng với vô vàn tiện ích sống cực kỳ phong phú của khu Đông Sài Gòn. Opal Boulevard nổi bật với phong cách kiến trúc hiện đại với hai khối tháp vững chãi vươn lên trời cao giữa một thành phố năng động.
Lấy cảm hứng từ vẻ đẹp lung linh của những viên đá quý, kết hợp với ý tưởng kiến tạo một thành phố năng động và hiện đại, Opal Boulevard mang vẻ đẹp rực rỡ, thời thượng của một khu đô thị sầm uất trên đại lộ đẹp nhất Tp.HCM. Nơi đây chính là chốn an cư lý tưởng kết nối cộng đồng sống văn minh của thời đại 4.0''',
                          style: AppTextTheme.w500_16px,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          child: Text(
                            'Tags',
                            style: AppTextTheme.w500_16px,
                          ),
                        ),
                        Row(
                          children: [
                            NewsTag(tag: 'Tin BĐS'),
                            const SizedBox(width: 12),
                            NewsTag(
                              tag: 'Opal Boulevard',
                            ),
                          ],
                        ),
                        const SizedBox(height: 20),
                      ],
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    height: 5,
                    color: AppColors.grey4,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: PixelConstant.marginHorizontal,
                        vertical: 16),
                    child: Text(
                      'Có thể bạn quan tâm',
                      style: AppTextTheme.w700_16px,
                    ),
                  ),
                  _Item(
                    houseModel: MockConst.mockHouses()[2],
                  ),
                  const SizedBox(height: 25),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final HouseModel houseModel;

  const _Item({Key key, this.houseModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Routes.instance.navigateTo(RouteName.projectDetailScreen);
      },
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          width: ScreenUtil.screenWidthDp,
          margin:
              EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
          decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: AppColors.grey4, width: 1)),
          height: 145,
          child: Row(
            children: [
              CustomImageNetwork(
                url: houseModel.background,
                width: 145,
                height: 145,
                border: 8,
                fit: BoxFit.cover,
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.all(12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 100,
                      height: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          gradient: CommonUtil.getGradient(
                              colors: AppColors.colorsGradient)),
                      child: Center(
                        child: Text(
                          houseModel.cost,
                          style: AppTextTheme.w500_14px
                              .copyWith(color: AppColors.white),
                        ),
                      ),
                    ),
                    const SizedBox(height: 8),
                    Text(
                      'Chung Cu 2PN - Ocean Park',
                      style: AppTextTheme.w500_12px.copyWith(fontSize: 13),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                    const SizedBox(height: 12),
                    Row(
                      children: [
                        SvgPicture.asset(
                          IconConst.searchLocation,
                          width: 14,
                          height: 14,
                          color: AppColors.grey,
                        ),
                        const SizedBox(width: 4),
                        Expanded(
                          child: Text(
                            houseModel.address,
                            style: AppTextTheme.w400_12px
                                .copyWith(color: AppColors.grey),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 12),
                    Row(
                      children: [
                        SvgPicture.asset(
                          IconConst.searchDetailBedRoom,
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          '3',
                          style: AppTextTheme.w500_14px
                              .copyWith(color: AppColors.grey7, fontSize: 12),
                        ),
                        const SizedBox(width: 16),
                        SvgPicture.asset(
                          IconConst.searchDetailBathRoom,
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          '2',
                          style: AppTextTheme.w500_14px
                              .copyWith(color: AppColors.grey7, fontSize: 12),
                        ),
                        const SizedBox(width: 16),
                        SvgPicture.asset(
                          IconConst.searchDetailArea,
                          width: 20,
                          height: 20,
                        ),
                        Text(
                          '170 M',
                          style: AppTextTheme.w500_14px
                              .copyWith(color: AppColors.grey7, fontSize: 12),
                        ),
                      ],
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
