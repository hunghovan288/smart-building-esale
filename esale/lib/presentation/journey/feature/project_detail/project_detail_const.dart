class ProjectDetailConst{
  static const String _path ='ProjectDetailConst.';
  static const String serviceUtils ='${_path}serviceUtils';
  static const String floorArea ='${_path}floorArea';
  static const String landArea ='${_path}landArea';
  static const String scale ='${_path}scale';
  static const String density ='${_path}density';
  static const String otherInformation ='${_path}otherInformation';

}