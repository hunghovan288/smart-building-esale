import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/journey/feature/project_detail/project_detail_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ProjectDetailOtherInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 12),
        Text(
          translate(ProjectDetailConst.otherInformation),
          style: AppTextTheme.w700_16px,
        ),
        const SizedBox(height: 8),
        _ItemOtherInfo(
          text: StringConst.rules,
          icon: IconConst.projectRules,
          onTap: () {
            Routes.instance.navigateTo(RouteName.rulesScreen);
          },
        ),
        _ItemOtherInfo(
          text: StringConst.contract,
          icon: IconConst.projectContract,
          onTap: () {
            Routes.instance.navigateTo(RouteName.contractScreen);
          },
        ),
        _ItemOtherInfo(
          text: StringConst.legalRecords,
          icon: IconConst.projectLegalRecords,
          onTap: () {
            Routes.instance.navigateTo(RouteName.legalRecordsScreen);
          },
        ),
        _ItemOtherInfo(
          text: StringConst.guarantee,
          icon: IconConst.projectGuarente,
        ),
      ],
    );
  }
}

class _ItemOtherInfo extends StatelessWidget {
  final String icon;
  final String text;
  final Function onTap;

  const _ItemOtherInfo({Key key, this.icon, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 40,
        child: Row(
          children: [
            icon.contains('.png')
                ? Image.asset(
                    icon,
                    width: 20,
                    height: 20,
                  )
                : SvgPicture.asset(
                    icon,
                    width: 20,
                    height: 20,
                  ),
            const SizedBox(width: 16),
            Text(
              translate(text) ?? '',
              style: AppTextTheme.w400_14px.copyWith(fontSize: 16),
            ),
            const Spacer(),
            SvgPicture.asset(
              IconConst.right,
              width: 14,
              height: 14,
            )
          ],
        ),
      ),
    );
  }
}
