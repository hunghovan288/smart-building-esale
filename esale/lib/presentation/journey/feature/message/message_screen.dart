import 'package:esale/common/const/mock_const.dart';
import 'package:esale/presentation/journey/feature/message/widgets/message_appbar.dart';
import 'package:esale/presentation/journey/feature/message/widgets/message_item.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class MessageScreen extends StatefulWidget {
  @override
  _MessageScreenState createState() => _MessageScreenState();
}

class _MessageScreenState extends State<MessageScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: Column(
        children: [
          MessageAppBar(),
          Divider(
            height: 1,
            color: AppColors.grey5,
          ),
          Expanded(
              child: ListView.builder(
                padding: EdgeInsets.only(top: 0),
            itemBuilder: (_, index) => MessageItem(
              messageModel: MockConst.mockMessage[index],
            ),
            itemCount: MockConst.mockMessage.length,
          ))
        ],
      ),
    );
  }
}
