import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/data/models/message_model.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';

class MessageItem extends StatelessWidget {
  final MessageModel messageModel;

  const MessageItem({Key key, this.messageModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: PixelConstant.marginHorizontal),
            child: CustomImageNetwork(
              url: messageModel.url,
              fit: BoxFit.cover,
              width: 52,
              height: 52,
              border: 26,
            ),
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          messageModel.userName,
                          style: AppTextTheme.w700_16px,
                        ),
                        const SizedBox(height: 8),
                        Text(
                          messageModel.message,
                          style: AppTextTheme.w400_12px,
                        ),
                      ],
                    )),
                    Text(
                      messageModel.time,
                      style: AppTextTheme.w400_12px,
                    ),
                    const SizedBox(width: 16),
                  ],
                ),
              ),
              Divider(
                color: AppColors.grey5,
                height: 2,
              )
            ],
          ))
        ],
      ),
    );
  }
}
