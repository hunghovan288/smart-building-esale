import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class MessageAppBar extends StatelessWidget {
  void _onSearch() {}

  void _onMore() {}

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: defaultAppbar + ScreenUtil.statusBarHeight,
      alignment: Alignment.bottomCenter,
      child: Row(
        children: [
          const SizedBox(width: 96),
          Expanded(
              child: Text(
            translate(StringConst.message),
            textAlign: TextAlign.center,
            style: AppTextTheme.w700_16px,
          )),
          InkWell(
            onTap: _onSearch,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 16, right: 8, bottom: 12, top: 12),
              child: SvgPicture.asset(
                IconConst.bottomSearch,
                width: 24,
                height: 24,
                color: AppColors.black,
              ),
            ),
          ),
          InkWell(
            onTap: _onMore,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 8, right: 16, bottom: 12, top: 12),
              child: Icon(
                Icons.more_vert,
                size: 24,
                color: AppColors.black,
              ),
            ),
          )
        ],
      ),
    );
  }
}
