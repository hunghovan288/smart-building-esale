import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/data/models/person_model.dart';
import 'package:esale/presentation/journey/container/container_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class CustomerItem extends StatelessWidget {
  final PersonModel personModel;

  const CustomerItem({Key key, this.personModel}) : super(key: key);

  void _onTap() {
    Routes.instance.navigateTo(RouteName.userDetailScreen);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onTap,
      child: Container(
        width: double.infinity,
        height: 152,
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(8),
            boxShadow: CommonConst.defaultShadow),
        margin:
            EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        child: Column(
          children: [
            Row(
              children: [
                Stack(
                  children: [
                    Column(
                      children: [
                        CustomImageNetwork(
                          url: personModel.url,
                          width: 52,
                          height: 52,
                          border: 26,
                          fit: BoxFit.cover,
                        ),
                        const SizedBox(height: 10),
                      ],
                    ),
                    Positioned.fill(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: SvgPicture.asset(
                          IconConst.medal,
                          width: 16,
                          height: 20,
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        personModel.name,
                        style: AppTextTheme.w500_16px,
                      ),
                      const SizedBox(height: 6),
                      Text(
                        personModel.status,
                        style: AppTextTheme.w400_14px,
                      )
                    ],
                  ),
                ),
                Container(
                  width: 52,
                  height: 36,
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.grey4, width: 1),
                      borderRadius: BorderRadius.circular(16)),
                  child: Center(
                    child: SvgPicture.asset(
                      IconConst.arrowRight,
                      width: 12,
                      height: 12,
                    ),
                  ),
                )
              ],
            ),
            const Spacer(),
            Row(
              children: [
                const SizedBox(width: 16),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '${personModel.numberMeet}',
                        style: AppTextTheme.w600_16px,
                      ),
                      const SizedBox(height: 4),
                      Text(
                        'lần gặp',
                        style: AppTextTheme.w500_12px
                            .copyWith(color: AppColors.grey),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: 115,
                  height: 36,
                  decoration: BoxDecoration(
                    gradient: CommonUtil.getGradient(),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        IconConst.bottomChat,
                        width: 16,
                        height: 16,
                        color: AppColors.white,
                      ),
                      const SizedBox(width: 8),
                      Text(
                        translate(ContainerConst.message),
                        style: AppTextTheme.w400_12px
                            .copyWith(color: AppColors.white),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
