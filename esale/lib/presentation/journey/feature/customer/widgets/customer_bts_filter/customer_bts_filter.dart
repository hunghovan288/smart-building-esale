import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/const/string_const.dart';
import 'package:esale/presentation/journey/feature/customer/widgets/customer_bts_filter/custom_bts_filter_item.dart';
import 'package:esale/presentation/journey/feature/customer/widgets/customer_bts_filter/customer_bts_filter_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/bottom_sheet_container.dart';
import 'package:esale/presentation/widgets/custom_cupertino_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class CustomBtsFilter extends StatelessWidget {
  void _onBack() {
    Routes.instance.pop();
  }

  @override
  Widget build(BuildContext context) {
    return BottomSheetContainer(
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  translate(CustomerBtsFilterConst.place),
                  style: AppTextTheme.w400_14px,
                ),
                const Spacer(),
                SvgPicture.asset(
                  IconConst.searchLocation,
                  width: 20,
                  height: 20,
                  color: AppColors.primaryColor,
                )
              ],
            ),
            const SizedBox(height: 12),
            Divider(
              color: AppColors.grey5,
            ),
            CustomBtsFilterItem(
              text: CustomerBtsFilterConst.sortByName,
              onTap: _onBack,
            ),
            CustomBtsFilterItem(
              text: CustomerBtsFilterConst.sortByIndex,
              onTap: _onBack,
            ),
            CustomBtsFilterItem(
              selected: true,
              text: CustomerBtsFilterConst.sortByPriority,
              onTap: _onBack,
            ),
            Row(
              children: [
                Text(
                  translate(CustomerBtsFilterConst.onlyMeet),
                  style: AppTextTheme.w500_14px,
                ),
                const Spacer(),
                CustomCupertinoSwitch(),
              ],
            )
          ],
        ),
      ),
    );
  }
}
