import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_check_box.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_translate/flutter_translate.dart';

class CustomBtsFilterItem extends StatelessWidget {
  final bool selected;
  final String text;
  final Function onTap;

  const CustomBtsFilterItem(
      {Key key, this.selected = false, this.text, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 52,
        child: Row(
          children: [
            GradientText(
              text: translate(text) ?? '',
              style: AppTextTheme.w400_14px,
              colors: selected
                  ? AppColors.colorsGradient
                  : [AppColors.black, AppColors.black],
            ),
            const Spacer(),
            CustomCheckBox(
              selected: selected,
            ),
          ],
        ),
      ),
    );
  }
}
