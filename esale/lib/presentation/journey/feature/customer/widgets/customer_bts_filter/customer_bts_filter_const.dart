class CustomerBtsFilterConst{
  static const String _path ='CustomerBtsFilterConst.';
  static const String place ='${_path}place';
  static const String sortByName ='${_path}sortByName';
  static const String sortByIndex ='${_path}sortByIndex';
  static const String sortByPriority ='${_path}sortByPriority';
  static const String onlyMeet ='${_path}onlyMeet';

}