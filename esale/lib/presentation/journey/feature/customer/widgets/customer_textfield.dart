import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/log_util.dart';
import 'package:esale/presentation/journey/feature/search_detail/search_detail_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class CustomerTextField extends StatelessWidget {
  final Function onBack;
  final Function onFilter;

  const CustomerTextField({Key key, this.onBack, this.onFilter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      margin: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
      width: double.infinity,
      decoration: BoxDecoration(
          color: AppColors.white,
          boxShadow: CommonConst.defaultShadow,
          borderRadius: BorderRadius.circular(8)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(width: 52),
          Expanded(
              child: TextFormField(
                  style: AppTextTheme.w400_14px,
                  cursorColor: AppColors.primaryColor,
                  textAlign: TextAlign.center,
                  decoration: InputDecoration(
                    hintText: translate(SearchDetailConst.search),
                    filled: true,
                    fillColor: Colors.white,
                    hintStyle:
                        AppTextTheme.w400_14px.copyWith(color: AppColors.grey7),
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(bottom: 4)
                  ))),
          InkWell(
            onTap: onFilter,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
              child: Image.asset(
                IconConst.filterPng,
                width: 20,
                height: 20,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
