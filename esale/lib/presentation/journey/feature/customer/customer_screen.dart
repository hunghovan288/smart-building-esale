import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/customer/widgets/customer_bts_filter/customer_bts_filter.dart';
import 'package:esale/presentation/journey/feature/customer/widgets/customer_item.dart';
import 'package:esale/presentation/journey/feature/customer/widgets/customer_textfield.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class CustomerScreen extends StatefulWidget {
  @override
  _CustomerScreenState createState() => _CustomerScreenState();
}

class _CustomerScreenState extends State<CustomerScreen> {
  void _onFilter() {
    CommonUtil.showCustomBottomSheet(
        context: context, child: CustomBtsFilter(), height: 300);
  }

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: true,
      body: Column(
        children: [
          SizedBox(height: ScreenUtil.statusBarHeight + 20),
          CustomerTextField(
            onFilter: _onFilter,
          ),
          Expanded(
            child: ListView.separated(
              itemBuilder: (_, index) => CustomerItem(
                personModel: MockConst.mockPerson[index],
              ),
              separatorBuilder: (_, i) => const SizedBox(height: 20),
              itemCount: MockConst.mockPerson.length,
            ),
          ),

        ],
      ),
    );
  }
}
