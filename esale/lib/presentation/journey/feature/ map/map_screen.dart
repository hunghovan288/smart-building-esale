import 'package:esale/common/const/mock_const.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/feature/%20map/widget/map_item.dart';
import 'package:esale/presentation/journey/feature/search_detail/widgets/search_detail_textfield.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:flutter/material.dart';

class MapScreen extends StatefulWidget {
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      autoDismissKeyboard: true,
      body: Stack(
        children: [
          Image.asset(
            MockConst.mockMapSearch,
            width: double.infinity,
            height: double.infinity,
            fit: BoxFit.cover,
          ),
          Column(
            children: [
              SizedBox(height: ScreenUtil.statusBarHeight + 20),
              SearchDetailTextField(),
              const Spacer(),
              Container(
                width: double.infinity,
                margin: EdgeInsets.only(bottom: 16),
                height: 140,
                child: ListView.separated(
                  padding: EdgeInsets.symmetric(horizontal: PixelConstant.marginHorizontal),
                  itemBuilder: (_, index) => MapItem(
                    houseModel: MockConst.mockHouses()[index],
                  ),
                  separatorBuilder: (_, i) => const SizedBox(width: 16),
                  itemCount: MockConst.mockHouses().length,
                  scrollDirection: Axis.horizontal,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
