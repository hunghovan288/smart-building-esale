class ContainerConst{
  static const String _path = 'ContainerConst.';
  static const String search = '${_path}search';
  static const String customer = '${_path}customer';
  static const String message = '${_path}message';
  static const String account = '${_path}account';
}