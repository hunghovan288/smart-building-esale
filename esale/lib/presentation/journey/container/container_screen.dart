import 'package:esale/presentation/journey/feature/account/account_screen.dart';
import 'package:esale/presentation/journey/feature/customer/customer_screen.dart';
import 'package:esale/presentation/journey/feature/message/message_screen.dart';
import 'package:esale/presentation/journey/feature/search/search_screen.dart';
import 'package:esale/presentation/widgets/bottom_nagivation_widget.dart';
import 'package:esale/presentation/widgets/custom_scafford.dart';
import 'package:esale/presentation/widgets/keep_alive_widget.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ScreenContainer extends StatelessWidget {
  List<String> icons = [
    'bottom_search.svg',
    'bottom_customer.svg',
    'bottom_chat.svg',
    'bottom_account.svg',
  ];

  @override
  Widget build(BuildContext context) {
    return CustomScaffold(
      body: BottomNavigation(
        icons: icons,
        tabViews: [
          KeepAliveWidget(child: SearchScreen()),
          KeepAliveWidget(child: CustomerScreen()),
          KeepAliveWidget(child: MessageScreen()),
          KeepAliveWidget(child: AccountScreen()),
        ],
      ),
    );
  }
}
