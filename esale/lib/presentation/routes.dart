import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/presentation/journey/auth/login/login_screen.dart';
import 'package:esale/presentation/journey/auth/splash/splash_screen.dart';
import 'package:esale/presentation/journey/container/container_screen.dart';
import 'package:esale/presentation/journey/feature/%20map/map_screen.dart';
import 'package:esale/presentation/journey/feature/account/edit_user/edit_user_screen.dart';
import 'package:esale/presentation/journey/feature/account/notification/notification_screen.dart';
import 'package:esale/presentation/journey/feature/house_detail/house_detail_screen.dart';
import 'package:esale/presentation/journey/feature/house_detail/installment/installment_screen.dart';
import 'package:esale/presentation/journey/feature/info/contract/contract_screen.dart';
import 'package:esale/presentation/journey/feature/info/detail_license/detail_license_screen.dart';
import 'package:esale/presentation/journey/feature/info/document/document_screen.dart';
import 'package:esale/presentation/journey/feature/info/document_detail/document_detail_screen.dart';
import 'package:esale/presentation/journey/feature/info/legal_records/legal_records_screen.dart';
import 'package:esale/presentation/journey/feature/info/rules/rules_screen.dart';
import 'package:esale/presentation/journey/feature/news/news_screen.dart';
import 'package:esale/presentation/journey/feature/project/project_screen.dart';
import 'package:esale/presentation/journey/feature/project_detail/project_detail_screen.dart';
import 'package:esale/presentation/journey/feature/search_detail/filter/filter_screen.dart';
import 'package:esale/presentation/journey/feature/search_detail/search_detail_screen.dart';
import 'package:esale/presentation/journey/feature/user_detail/pay_contract/pay_contract_screen.dart';
import 'package:esale/presentation/journey/feature/user_detail/user_detail_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  factory Routes() => _instance;

  Routes._internal();

  static final Routes _instance = Routes._internal();

  static Routes get instance => _instance;

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> popAndNavigateTo(
      {dynamic result, String routeName, dynamic arguments}) {
    return navigatorKey.currentState
        .popAndPushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> navigateAndRemove(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState.pushNamedAndRemoveUntil(
      routeName,
      (Route<dynamic> route) => false,
      arguments: arguments,
    );
  }

  dynamic popUntil() {
    return navigatorKey.currentState.popUntil((route) => route.isFirst);
  }

  Future<dynamic> navigateAndReplace(String routeName,
      {dynamic arguments}) async {
    return navigatorKey.currentState
        .pushReplacementNamed(routeName, arguments: arguments);
  }

  dynamic pop({dynamic result}) {
    return navigatorKey.currentState.pop(result);
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.newsScreen:
        return CupertinoPageRoute(
          builder: (context) => NewsScreen(),
        );
      case RouteName.notificationScreen:
        return CupertinoPageRoute(
          builder: (context) => NotificationScreen(),
        );
      case RouteName.editUserScreen:
        return CupertinoPageRoute(
          builder: (context) => EditUserScreen(),
        );
      case RouteName.payContractScreen:
        return CupertinoPageRoute(
          builder: (context) => PayContractScreen(),
        );
      case RouteName.userDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => UserDetailScreen(),
        );
      case RouteName.contractScreen:
        return CupertinoPageRoute(
          builder: (context) => ContractScreen(),
        );
      case RouteName.rulesScreen:
        return CupertinoPageRoute(
          builder: (context) => RulesScreen(),
        );
      case RouteName.legalRecordsScreen:
        return CupertinoPageRoute(
          builder: (context) => LegalRecordsScreen(),
        );
      case RouteName.documentDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => DocumentDetailScreen(),
        );
      case RouteName.documentScreen:
        return CupertinoPageRoute(
          builder: (context) => DocumentScreen(),
        );
      case RouteName.detailLicenseScreen:
        return CupertinoPageRoute(
          builder: (context) => DetailLicenseScreen(),
        );
      case RouteName.projectDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => ProjectDetailScreen(),
        );
      case RouteName.projectScreen:
        return CupertinoPageRoute(
          builder: (context) => ProjectScreen(),
        );
      case RouteName.installmentScreen:
        return CupertinoPageRoute(
          builder: (context) => InstallmentScreen(),
        );
      case RouteName.mapScreen:
        return CupertinoPageRoute(
          builder: (context) => MapScreen(),
        );
      case RouteName.houseDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => HouseDetailScreen(),
        );
      case RouteName.filterScreen:
        return CupertinoPageRoute(
          builder: (context) => FilterScreen(),
        );
      case RouteName.searchDetailScreen:
        return CupertinoPageRoute(
          builder: (context) => SearchDetailScreen(),
        );
      case RouteName.splashScreen:
        return CupertinoPageRoute(
          builder: (context) => SplashScreen(),
        );
      case RouteName.screenContainer:
        return CupertinoPageRoute(
          builder: (context) => ScreenContainer(),
        );
      case RouteName.loginScreen:
        return CupertinoPageRoute(
          builder: (context) => LoginScreen(),
        );
      default:
        return _emptyRoute(settings);
    }
  }

  static MaterialPageRoute _emptyRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => Scaffold(
        backgroundColor: Colors.green,
        appBar: AppBar(
          leading: InkWell(
            onTap: () => Navigator.of(context).pop(),
            child: const Center(
              child: Text(
                'Back',
                style: TextStyle(fontSize: 16),
              ),
            ),
          ),
        ),
        body: Center(
          child: Text('No path for ${settings.name}'),
        ),
      ),
    );
  }
}
