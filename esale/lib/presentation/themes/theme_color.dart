import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xff2058DB);
  static const Color primaryLight = Color(0xff1086F3);
  static const Color primaryDart = Color(0xff134CDF);
  static const Color primaryColorText = Color(0xff2058DB);
  static const Color grey3 = Color(0xffF3F3F3);
  static const Color grey4 = Color(0xffE8E8E8);
  static const Color grey6 = Color(0xffBFBFBF);
  static const Color grey5 = Color(0xffD9D9D9);
  static const Color grey7 = Color(0xff8C8C8C);
  static const Color grey9 = Color(0xff1d2330);
  static const Color black = Colors.black;
  static const Color blue = Color(0xff1890FF);
  static const Color white = Colors.white;
  static const Color pink = Color(0xffEA3C71);
  static const Color yellow = Color(0xffF6A923);
  static const Color green = Color(0xff00AB6B);

  static Color grey = black.withOpacity(0.5);
  static const List<Color> colorsGradient = [primaryLight, primaryDart];
}
