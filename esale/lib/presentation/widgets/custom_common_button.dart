import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class CustomCommonButton extends StatelessWidget {
  final String text;
  final Function onTap;
  const CustomCommonButton({Key key, this.text,this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: double.infinity,
        height: 44,
        decoration: BoxDecoration(
          gradient: CommonUtil.getGradient(colors: [
            Color(0xff1086F3),
            Color(0xff134CDF),
          ]),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: Text(
            text ?? '',
            style: AppTextTheme.w700_14px.copyWith(color: AppColors.white),
          ),
        ),
      ),
    );
  }
}
