import 'package:cached_network_image/cached_network_image.dart';
import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/enum.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/common_util.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/data/models/house_model.dart';
import 'package:esale/presentation/journey/feature/search/search_const.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:esale/presentation/widgets/custom_cache_image_network.dart';
import 'package:esale/presentation/widgets/custom_image_network.dart';
import 'package:esale/presentation/widgets/custom_outline_border_text.dart';
import 'package:esale/presentation/widgets/gradient_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

class ItemHouse extends StatelessWidget {
  final HouseModel houseModel;

  const ItemHouse({Key key, this.houseModel}) : super(key: key);
  void _onItemTap(){
    Routes.instance.navigateTo(RouteName.houseDetailScreen);
  }
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: _onItemTap,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: AppColors.grey4, width: 1),
            boxShadow: [
              BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 3,
                  blurRadius: 5,
                  offset: Offset(0, 5))
            ],
          ),
          // height: 300,
          width: ScreenUtil.screenWidthDp * 0.8,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomImageNetwork(
                url: houseModel.background,
                width: double.infinity,
                height: 180,
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 16),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    OutLineBorderText(
                      text: houseModel.cost ?? '',
                      gradient: CommonUtil.getGradient(
                          colors: AppColors.colorsGradient),
                      strokeWidth: 2,
                      radius: 6,
                      child: GradientText(
                        text: houseModel.cost ?? '',
                        colors: AppColors.colorsGradient,
                        style: AppTextTheme.w600_16px.copyWith(fontSize: 12),
                      ),
                    ),
                    const SizedBox(height: 16),
                    GradientText(
                      text:
                          '${houseModel.numberRoom}${translate(SearchConst.bedroom)} - ${houseModel.name}',
                      colors: AppColors.colorsGradient,
                      style: AppTextTheme.w600_16px,
                      gradientDirection: GradientDirection.ttb,
                    ),
                    const SizedBox(height: 8),
                    Row(
                      children: [
                        SvgPicture.asset(
                          IconConst.searchLocation,
                          width: 14,
                          height: 14,
                        ),
                        const SizedBox(width: 4),
                        Text(
                          houseModel.address ?? '',
                          style: AppTextTheme.w400_14px,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
