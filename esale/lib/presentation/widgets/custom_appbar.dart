import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/const/pixel_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

const double defaultAppbar = 56.0;

class CustomAppBar extends StatelessWidget {
  final Function onBack;
  final Widget widgetRight;
  final String title;
  final Color colorIconBack;
  final Color backgroundColor;
  final Color colorTitle;
  final bool gradient;
  final String iconRight;
  final String iconLeft;

  const CustomAppBar({
    Key key,
    this.onBack,
    this.colorIconBack,
    this.backgroundColor,
    this.colorTitle,
    this.widgetRight,
    this.gradient = true,
    this.title,
    this.iconRight,
    this.iconLeft,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: ScreenUtil.statusBarHeight),
      height: defaultAppbar + ScreenUtil.statusBarHeight,
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          onBack != null
              ? InkWell(
                  onTap: onBack,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: PixelConstant.marginHorizontal),
                    child: Container(
                      width: 48,
                      height: 48,
                      padding: EdgeInsets.only(top: 14, bottom: 14, right: 20),
                      child: SvgPicture.asset(
                        iconLeft ?? IconConst.arrowBack,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                )
              : const SizedBox(),
          Expanded(
              child: Text(
            title ?? '',
            style: AppTextTheme.bold16PxW700.copyWith(
              color: colorTitle??AppColors.black,
              fontWeight: FontWeight.w500,
            ),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.center,
          )),
          widgetRight ??
              (iconRight != null
                  ? InkWell(
                      onTap: onBack,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: PixelConstant.marginHorizontal, right: 6),
                        child: Container(
                          width: 48,
                          height: 48,
                          child: Center(
                            child: SvgPicture.asset(
                              iconRight,
                              width: 24,
                              height: 24,
                            ),
                          ),
                        ),
                      ),
                    )
                  : const SizedBox(
                      width: 64,
                    )),
        ],
      ),
    );
  }
}
