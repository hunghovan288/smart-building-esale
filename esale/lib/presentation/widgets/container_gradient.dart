import 'package:esale/common/utils/common_util.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';

class ContainerGradient extends StatelessWidget {
  final String text;
  final Widget child;
  final double width;
  const ContainerGradient({Key key, this.text,this.child,this.width = 93}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ,
      height: 30,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          gradient: CommonUtil.getGradient(colors: AppColors.colorsGradient)),
      child: Center(
        child:Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            child?? Text(
              text ?? '',
              style: AppTextTheme.w500_14px.copyWith(color: AppColors.white),
            ),
          ],
        ),
      ),
    );
  }
}
