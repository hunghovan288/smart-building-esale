import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';

class StyleSlider extends StatelessWidget {
  final Widget child;

  const StyleSlider({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SliderTheme(
      data: SliderTheme.of(context).copyWith(
        trackHeight: 1.0,
        inactiveTrackColor: AppColors.grey4,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 13.0),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 30.0),
      ),
      child: child,
    );
  }
}
