import 'package:esale/common/const/common_constant.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/journey/container/container_const.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_translate/flutter_translate.dart';

typedef OnTabChanged = void Function(int index);

const heightItem = 58.0;

class BottomNavigation extends StatefulWidget {
  final List<Widget> tabViews;
  final List<String> icons;
  final Color activeColor;
  final Color inActiveColor;
  final double iconSize;
  final OnTabChanged onTabChanged;
  final int initIndex;
  final int countItem;
  final Function iconCenterTap;

  BottomNavigation({
    Key key,
    @required this.tabViews,
    @required this.icons,
    this.initIndex = 0,
    this.activeColor = AppColors.primaryColor,
    this.inActiveColor = AppColors.grey6,
    this.iconSize = 24.0,
    this.countItem = 0,
    this.iconCenterTap,
    this.onTabChanged,
  })  : assert(tabViews != null, 'Tab view not be null'),
        assert(icons != null, 'Icons not be null'),
        super(key: key);

  @override
  State<StatefulWidget> createState() => BottomNavigationState();
}

class BottomNavigationState extends State<BottomNavigation> {
  int selectedIndex;
  List<String> _icon;

  @override
  void initState() {
    selectedIndex = widget.initIndex;
    _icon = widget.icons;
    super.initState();
  }

  void changeToTabIndex(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    final tabs = _icon.asMap().entries.map(
      (entry) {
        final index = entry.key;
        final source = entry.value;
        final isSelected = index == selectedIndex;
        return source != null
            ? Expanded(
                child: Material(
                  color: Colors.white,
                  child: InkWell(
                    highlightColor: AppColors.grey6,
                    splashColor: AppColors.grey6,
                    onTap: () {
                      if (!isSelected) {
                        changeToTabIndex(index);
                        if (widget.onTabChanged != null) {
                          widget.onTabChanged(selectedIndex);
                        }
                      }
                    },
                    child: Container(
                      height: heightItem,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            '${IconConst.path}$source',
                            width: widget.iconSize,
                            height: widget.iconSize,
                            fit: BoxFit.cover,
                            color: isSelected
                                ? widget.activeColor
                                : widget.inActiveColor,
                          ),
                          Text(
                            translate(_mapIndexToString(index)),
                            style: AppTextTheme.smallGrey.copyWith(
                                color: isSelected
                                    ? AppColors.primaryColor
                                    : AppColors.grey7,
                                fontWeight: FontWeight.w400),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : const Spacer();
      },
    ).toList();

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: IndexedStack(
        index: selectedIndex,
        children: widget.tabViews,
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        child: Row(
          children: tabs,
          crossAxisAlignment: CrossAxisAlignment.end,
        ),
      ),
    );
  }

  String _mapIndexToString(int index) {
    switch (index) {
      case 0:
        return ContainerConst.search;
      case 1:
        return ContainerConst.customer;
      case 2:
        return ContainerConst.message;
      default:
        return ContainerConst.account;
    }
  }
}
