import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/presentation/app.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:esale/presentation/themes/theme_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomSheetContainer extends StatelessWidget {
  final Widget child;
  final String title;
  final Function onLeftTap;
  final Function onRightTap;
  final String textButtonRight;
  final Color backgroundColor;

  const BottomSheetContainer({
    Key key,
    this.child,
    this.title,
    this.onLeftTap,
    this.backgroundColor,
    this.onRightTap,
    this.textButtonRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12.0),
          topRight: Radius.circular(12.0),
        ),
        color: backgroundColor ?? Colors.white,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: 40,
            margin: EdgeInsets.only(top: 12, bottom: 16),
            height: 4,
            decoration: BoxDecoration(
                color: AppColors.grey6, borderRadius: BorderRadius.circular(2)),
          ),
          Expanded(child: child),
        ],
      ),
    );
  }
}
