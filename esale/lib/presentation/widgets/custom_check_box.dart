import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';

class CustomCheckBox extends StatelessWidget {
  final bool selected;

  const CustomCheckBox({Key key, this.selected = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 20,
      height: 20,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: selected
              ? Border.all(color: AppColors.primaryColor, width: 4)
              : Border.all(color: AppColors.grey4, width: 1),
          color: selected ? AppColors.white : AppColors.grey3),
    );
  }
}
