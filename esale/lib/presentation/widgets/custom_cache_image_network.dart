import 'package:cached_network_image/cached_network_image.dart';
import 'package:esale/common/const/icon_constant.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:shimmer/shimmer.dart';

class CustomCacheImageNetwork extends StatelessWidget {
  final String url;
  final double width;
  final double height;
  final BoxFit fit;
  final double border;

  const CustomCacheImageNetwork({
    Key key,
    @required this.url,
    this.width = 40,
    this.height = 40,
    this.fit,
    this.border,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if ((url ?? '').isEmpty) {
      return _widgetImagePlaceHolder();
    }
    if (url.contains('http')) {
      return ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(border ?? 0)),
        child: Column(
          children: [
            CachedNetworkImage(
              imageUrl: url,
              fit: fit,
              width: width,
              height: height,
              errorWidget: (_, url, error) => _widgetImagePlaceHolder(),
              placeholder: (context, url) => Shimmer.fromColors(
                baseColor: Colors.grey[300],
                highlightColor: Colors.grey[100],
                enabled: true,
                child: Container(
                  width: width,
                  height: height,
                  color: AppColors.white,
                ),
              ),
            ),
          ],
        ),
      );
    }
    return _widgetImagePlaceHolder();
  }

  Widget _widgetImagePlaceHolder() => Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(8)),
          border: Border.all(color: AppColors.grey4),
        ),
        padding: EdgeInsets.all(16),
        child: Center(
          child: SvgPicture.asset(
            IconConst.imagePlaceHolder,
            width: double.infinity,
            height: double.infinity,
            color: AppColors.grey4,
          ),
        ),
      );
}
