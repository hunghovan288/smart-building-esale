import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomCupertinoSwitch extends StatefulWidget {
  @override
  _CustomCupertiSwitchState createState() => _CustomCupertiSwitchState();
}

class _CustomCupertiSwitchState extends State<CustomCupertinoSwitch> {
  bool _isShow = true;

  void _onChangeShow(bool value) {
    setState(() {
      _isShow = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: 0.9,
      origin: Offset(40, 0),
      child: CupertinoSwitch(
        onChanged: _onChangeShow,
        activeColor: AppColors.primaryColor,
        value: _isShow,
      ),
    );
  }
}
