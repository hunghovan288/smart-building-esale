import 'package:esale/common/navigation/route_names.dart';
import 'package:esale/common/utils/screen_utils.dart';
import 'package:esale/presentation/routes.dart';
import 'package:esale/presentation/themes/theme_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_translate/localized_app.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    final localizationDelegate = LocalizedApp.of(context).delegate;
    return MaterialApp(
      navigatorKey: Routes.instance.navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'Smart Building Application',
      onGenerateRoute: Routes.generateRoute,
      initialRoute: RouteName.splashScreen,
      theme: ThemeData(
          primaryColor: AppColors.primaryColor,
          fontFamily: 'Roboto',
          canvasColor: Colors.transparent,
          platform: TargetPlatform.iOS,
          pageTransitionsTheme: const PageTransitionsTheme(builders: {
            TargetPlatform.android: CupertinoPageTransitionsBuilder(),
            TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          })),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        localizationDelegate
      ],
      supportedLocales: localizationDelegate.supportedLocales,
      locale: localizationDelegate.currentLocale,
      // locale: Locale('en'),
      builder: (context, widget) {
        ScreenUtil.init(context);
        return widget;
      },
    );
  }
}
