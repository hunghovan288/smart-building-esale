class MessageModel{
  final String url;
  final String userName;
  final String message;
  final String time;

  MessageModel({this.url, this.userName, this.message, this.time});
}