class PersonModel{
  final String url;
  final String name;
  final int numberMeet;
  final String status;

  PersonModel({this.url, this.name, this.numberMeet, this.status});
}