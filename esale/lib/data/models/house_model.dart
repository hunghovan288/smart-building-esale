class HouseModel {
  final String background;
  final String cost;
  final int numberRoom;
  final String name;
  final String address;
  final List<String> avatarPerson;
  final String installment;
  final int numberToilet;
  final String area;
  final String description;
  final String status;
  final String electrical;
  final String type;
  final String costPerMeter;
  final String yearBuilt;
  final String furniture;
  final String floor;
  final List<String> listImage;

  HouseModel({
    this.background,
    this.listImage,
    this.cost,
    this.numberRoom,
    this.name,
    this.address,
    this.avatarPerson,
    this.installment,
    this.numberToilet,
    this.area,
    this.description,
    this.status,
    this.electrical,
    this.type,
    this.costPerMeter,
    this.yearBuilt,
    this.furniture,
    this.floor,
  });
}
